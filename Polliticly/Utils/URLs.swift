//
//  URLs.swift
//  Polliticly
//
//  Created by Zan on 2020-10-04.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import Foundation
import UIKit

struct Constants {
    //App Constants
    //static let BASE_URL = "http://192.168.0.103";
    static let BASE_URL = "http://dev.polliticly.com"
    //static let BASE_URL = "http://beta.polliticly.com"
    
    //Login URL
    static let LOGIN_URL = BASE_URL + "/validateLogin";
    
    //Save or Edit Voter Information URL
    static let EDIT_VOTER_URL = BASE_URL + "/editVoter";
    
    //Change Password URL
    static let CHANGE_PASSWORD_URL = BASE_URL + "/changeVoterPassword";
    
    //Store Firebase token for Push Notification URL
    static let STORE_NOTIFICATION_TOKEN = BASE_URL + "/setPushToken";
    
    //Create a Poll
    static let CREATE_POLL = BASE_URL + "/createAPoll";
    
    //Get Poll information
    static let GET_POLLS = BASE_URL + "/getPolls";
    
    //Set Vote Selection
    static let SET_POLL_SELECTION = BASE_URL + "/castVote";
    
    //Add a Like to the Poll
    static let ADD_LIKE_URL = BASE_URL + "/addLike";
    
    //Remove a Like to the Poll
    static let REMOVE_LIKE_URL = BASE_URL + "/removeLike";

    //Add a Comment to the Poll
    static let ADD_COMMENT_URL = BASE_URL + "/addComment";
    
    //Remove a Comment to the Poll
    static let REMOVE_COMMENT_URL = BASE_URL + "/removeComment";
    
    //Add a Comment to the Poll
    static let GET_COMMENTS_URL = BASE_URL + "/getComments";

    //Message constituents
    static let MSG_REPS = BASE_URL + "/emailReps";
    
}
