//
//  myHTTPRequest.swift
//  Polliticly
//
//  Created by Zan on 2020-10-15.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import Foundation

struct myHTTPRequest {
 
     static func openURL(parameters: [String: Any], url: URL) {
    
        //create the session object
        let session = URLSession.shared

        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
                print(error.localizedDescription)
        }
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            print("Response -- ", response)
        })
        
        task.resume()
    }
    
}
