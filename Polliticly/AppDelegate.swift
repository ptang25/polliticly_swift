//
//  AppDelegate.swift
//  Polliticly
//
//  Created by Future Vision Tech  on 07/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import Firebase

import UIKit
import UIKit
import SVProgressHUD
import Firebase
import FirebaseAuth
import Toaster
import FirebaseDatabase
import IQKeyboardManagerSwift
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate  {

    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        IQKeyboardManager.shared.enable = true
        FirebaseApp.configure()
        
        //Dark Mode - Disable inverted colors for Images for entire App
        if #available(iOS 11.0, *) {
           UIImageView.appearance().accessibilityIgnoresInvertColors = true
           //UIButton.appearance().accessibilityIgnoresInvertColors = true
        }
        
        // [START set_messaging_delegate]
        Messaging.messaging().delegate = self
        // [END set_messaging_delegate]
        
        // Register for remote notifications. This shows a permission dialog on first run, to
        // show the dialog at a more appropriate time move this registration accordingly.
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self

            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            } else {
              let settings: UIUserNotificationSettings =
              UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
              application.registerUserNotificationSettings(settings)
        }
        // [END register_for_notifications]
    
        
        if !(UserDefaults.standard.string(forKey: "email")?.isEmpty ?? true) {
            
            let userID: Int? = UserDefaults.standard.integer(forKey: "userID");
            
            loginCheck(type: "auto")
    
        }
        else{
            loadLoginScreen()
        }
        
        //Firebase Notifications
        application.registerForRemoteNotifications()
        
        //Facebook SDK
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)

        return true
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    
    // [START receive_message]
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
          print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                       fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        // If you are receiving a notification message while your app is in the background,
        // this callback will not be fired till the user taps on the notification launching the application.
        // TODO: Handle data of notification
        // With swizzling disabled you must let Messaging know about the message, for Analytics
        // Messaging.messaging().appDidReceiveMessage(userInfo)
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
          print("Message ID: \(messageID)")
        }

        // Print full message.
        print(userInfo)

        completionHandler(UIBackgroundFetchResult.newData)
      }
      // [END receive_message]
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Unable to register for remote notifications: \(error.localizedDescription)")
      }

    // This function is added here only for debugging purposes, and can be removed if swizzling is enabled.
    // If swizzling is disabled then this function must be implemented so that the APNs token can be paired to
    // the FCM registration token.
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        print("APNs token retrieved: \(deviceToken)")

        // With swizzling disabled you must set the APNs token here.
        // Messaging.messaging().apnsToken = deviceToken
    }
    
    //
    // [Start Deep Link Functions]
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([UIUserActivityRestoring]?) -> Void) -> Bool {
      let handled = DynamicLinks.dynamicLinks().handleUniversalLink(userActivity.webpageURL!) { (dynamiclink, error) in
        // ...
      }

      return handled
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
      
        ApplicationDelegate.shared.application(app, open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        
        return application(app, open: url,
                         sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                         annotation: "")
    }

    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
      if let dynamicLink = DynamicLinks.dynamicLinks().dynamicLink(fromCustomSchemeURL: url) {
        // Handle the deep link. For example, show the deep-linked content or
        // For now this is blank because we are not really using Deep Link - we're just using Links to open App.
        // This is coded for in for future use
        return true
      }
      return false
    }
    // [END Deep Link Functions]
    
    
    func loadLoginScreen()
    {
        let viewControlller = LoginViewViewController(nibName: String(describing:  LoginViewViewController
            .self), bundle: nil)
        let navigationController = UINavigationController.init(rootViewController: viewControlller)
        
        self.window?.rootViewController = navigationController
        navigationController.navigationBar.isHidden = true
        self.window?.makeKeyAndVisible()
    }
    
    func loadDashboardViewController()
    {
        
        let tabBarCnt = UITabBarController()
        
        //Set Menu Select Color
        tabBarCnt.tabBar.tintColor = UIColor.init(hexString: "#1FD8B9");
        tabBarCnt.tabBar.backgroundColor = UIColor.white
        
        tabBarCnt.tabBar.layer.borderWidth = 0
        tabBarCnt.tabBar.layer.shadowOffset = CGSize(width: 0, height: 0)
        tabBarCnt.tabBar.layer.shadowRadius = 25
        tabBarCnt.tabBar.layer.shadowColor = UIColor.darkGray.cgColor
        tabBarCnt.tabBar.layer.shadowOpacity = 0.4

        //Polls
        let viewControlller = DashboardViewController(nibName: String(describing:  DashboardViewController.self), bundle: nil)
        let navigationController = UINavigationController.init(rootViewController: viewControlller)
        navigationController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Group 11161"), tag: 0)
        
        //Create Polls Controller
        let createPollsViewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CreatePollViewController") as! CreatePollViewController
        let createPollsNavigationController = UINavigationController.init(rootViewController: createPollsViewController)

        createPollsNavigationController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Group 70"), tag: 0)
        
        //Send Message Controller
        let sendMessageViewController:UIViewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "MessageViewController") as! MessageViewController
        let sendMessageNavigationController = UINavigationController.init(rootViewController: sendMessageViewController)

        sendMessageNavigationController.tabBarItem = UITabBarItem(title: "", image: UIImage(named: "Group 44"), tag: 0)
        
        //init tabbar
        tabBarCnt.viewControllers = [navigationController,createPollsNavigationController,sendMessageNavigationController]
        
        self.window?.rootViewController = tabBarCnt
        navigationController.navigationBar.isHidden = true
        createPollsNavigationController.navigationBar.isHidden = true
        sendMessageNavigationController.navigationBar.isHidden = true

        self.window?.makeKeyAndVisible()
    }
    
    func loginCheck(type: String) {
            Auth.auth().signIn(withEmail: UserDefaults.standard.string(forKey: "email") ?? "", password: UserDefaults.standard.string(forKey: "password") ?? "") { (user, error) in
                if error != nil {
                    SVProgressHUD.dismiss()
                    Toast(text: "Incorrect Email or Password!!!").show()
                }
                else {
                    //Sign In to Backend as well
                    let parameters = ["email" : UserDefaults.standard.string(forKey: "email"),
                                      "password" : UserDefaults.standard.string(forKey: "password")]
                    
                    //Sign In to Backend as well
                    let session = URLSession.shared
                    let url = URL(string: Constants.LOGIN_URL)!
                                        
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST" //set http method as POST
                    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                    request.addValue("application/json", forHTTPHeaderField: "Accept")

                    do {
                        request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                    } catch let error {
                            print(error.localizedDescription)
                    }
                    
                    //create dataTask using the session object to send data to the server
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                        guard error == nil else {
                            return
                        }

                        guard let data = data else {
                            return
                        }

                        print(response)
                        
                        do {
                            //create json object from data
                            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                            
                                //Get the return Code of the HTTP Request
                                let code = json["code"] as? Int;
                                    
                                //Successfully able to retrieve user from Database
                                if (code == 1) {
                                    let data = json["data"] as! [String:Any];
                                        
                                    //Set User Values
                                    UserDefaults.standard.setValue(data["id"], forKey: "userID")
                                    UserDefaults.standard.setValue(data["first_name"], forKey: "first_name")
                                    UserDefaults.standard.setValue(data["last_name"], forKey: "last_name")
                                    UserDefaults.standard.setValue(data["username"], forKey: "username")
                                    UserDefaults.standard.setValue(data["birthday"], forKey: "birthday")
                                    UserDefaults.standard.setValue(data["ethnicity"], forKey: "ethnicity")
                                    UserDefaults.standard.setValue(data["gender"], forKey: "gender")
                                    UserDefaults.standard.setValue(data["avatar"], forKey: "avatar")
                                    UserDefaults.standard.setValue(data["house_num"], forKey: "house_num")
                                    UserDefaults.standard.setValue(data["zipCode"], forKey: "zipCode")
                                    UserDefaults.standard.setValue(data["street"], forKey: "street")

                                }
                                
                            }
                        } catch let error {
                                print(error.localizedDescription)
                        }
                    })
                    
                    task.resume()
                    
                    
                    SVProgressHUD.dismiss()
                    self.loadDashboardViewController()
                }
            }
    }


}
extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}

// [START ios_10_message_handling]
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

  // Receive displayed notifications for iOS 10 devices.
  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              willPresent notification: UNNotification,
    withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
    let userInfo = notification.request.content.userInfo

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // Print full message.
    print(userInfo)

    // Change this to your preferred presentation option
    completionHandler([[.alert, .sound]])
  }

  func userNotificationCenter(_ center: UNUserNotificationCenter,
                              didReceive response: UNNotificationResponse,
                              withCompletionHandler completionHandler: @escaping () -> Void) {
    let userInfo = response.notification.request.content.userInfo
    // Print message ID.
    if let messageID = userInfo[gcmMessageIDKey] {
      print("Message ID: \(messageID)")
    }

    // With swizzling disabled you must let Messaging know about the message, for Analytics
    // Messaging.messaging().appDidReceiveMessage(userInfo)
    // Print full message.
    print(userInfo)

    completionHandler()
  }
}
// [END ios_10_message_handling]

extension AppDelegate : MessagingDelegate {
  // [START refresh_token]
  func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
    print("Firebase registration token: \(fcmToken)")
    print("Firebase registration token: ", UserDefaults.standard.integer(forKey: "userID"))
    
    //Set Token Value incase we are registering User for the first time
    UserDefaults.standard.setValue(fcmToken, forKey: "deviceToken")
    
    let dataDict:[String: String] = ["token": fcmToken!]
    NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
    // Note: This callback is fired at each app startup and whenever a new token is generated.
    
    //Add Token to Database
    let parameters = ["userID"            : UserDefaults.standard.integer(forKey: "userID"),
                      "token"             : fcmToken] as [String : Any]
    
    //create the url with URL
    let url = URL(string: Constants.STORE_NOTIFICATION_TOKEN)!

    //create the session object
    let session = URLSession.shared

    //now create the URLRequest object using the url object
    var request = URLRequest(url: url)
    request.httpMethod = "POST" //set http method as POST
    request.addValue("application/json", forHTTPHeaderField: "Content-Type")
    request.addValue("application/json", forHTTPHeaderField: "Accept")

    do {
        request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
    } catch let error {
            print(error.localizedDescription)
    }
    
    //create dataTask using the session object to send data to the server
    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

        guard error == nil else {
            return
        }

        guard let data = data else {
            return
        }

        print(response)
        
        /*do {
            //create json object from data
            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
            
                //Get the return Code of the HTTP Request
                let code = json["code"] as? Int;
                                            
                //Successfully able to change password
                if (code == 1) {
                        
                    //Post Toast
                    Toast(text: "Lo").show()
                }
            }
        } catch let error {
                print(error.localizedDescription)
        }*/
    })
    
    task.resume()
    
  }
  // [END refresh_token]
}
