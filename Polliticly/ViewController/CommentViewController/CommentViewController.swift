//
//  CommentViewController.swift
//  Polliticly
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import SVProgressHUD
import Firebase
import Toaster
import FirebaseDatabase
import FirebaseAuth

class CommentViewController: UIViewController {
    
    var userQuestionsAnsweredSelected: UserQuestionsAnsweredModel!
    var questionSelected: QuestionsModel!
    var commentsModel = [CommentsModel]()

    //The Number of people who voted for this Poll
    var numVoted: Int = Int()
    
    var ref: DatabaseReference!
    var ref2: DatabaseReference!
    var ref3: DatabaseReference!
        
    @IBOutlet weak var tblCommentView: UITableView!
    @IBOutlet weak var txtMessageView: GrowingTextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //NotificationCenter.default.post(name: .clenDatabaseReference, object: nil)
        
        
        // Do any additional setup after loading the view.
        if #available(iOS 13.0, *) {
                             
        }
        //All version under 13
        else {
            self.tblCommentView.borderColor1 = UIColor.darkGray;
        }
        
        self.getComments()
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    /**
     * Gets all the Comments from a given Poll Question
     */
    func getComments() {
        
        //Create JSON Object to send to Database
        let parameters = ["pollID"     : questionSelected.id]
        
        //create the url with URL
        let url = URL(string: Constants.GET_COMMENTS_URL)!

        //create the session object
        let session = URLSession.shared

        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
         
        //Reset the Model
        commentsModel = [CommentsModel]()
        
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
                print(error.localizedDescription)
        }
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            //print("Response -- ", response)
         
             do {
                 //create json object from data
                 if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                 
                     //Get the return Code of the HTTP Request
                     let code = json["code"] as? Int;
                         
                     //Successfully able to retrieve user from Database
                     if (code == 1) {
                        
                        let jsonArray = json["data"] as! [Any];
                        
                        for comment in jsonArray {
                         
                            let data = comment as! [String:Any];
                            
                            let commentID = data["id"] as! Int
                            let username  = data["username"] as? String
                            let comment   = data["comment"] as? String
                            let timeStamp = data["updated_at"] as? String
                            let userID      = data["voterID"] as! Int
                            
                            //Set User Values
                            let newCommentModel: CommentsModel = CommentsModel(id: commentID, name: username!, comment: comment ?? "", timeStamp: timeStamp ?? "", userID: String(userID), strTimeStamp: timeStamp ?? "")

                            self.commentsModel.append(newCommentModel)
                        }
                        
                        print("Num of Comments: ", self.commentsModel.count)
                        
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.questionSelected.comments = self.commentsModel.count
                            self.tblCommentView.reloadData()
                               
                            if self.commentsModel.count > 0 {
                                self.tblCommentView.scrollToRow(at: IndexPath(row: self.commentsModel.count - 1, section: 1), at: .bottom, animated: true)
                            }
                        }
                     }
                     
                 }
             } catch let error {
                     print(error.localizedDescription)
             }
        })
        
        task.resume()
        
    }
    
    func assignUserSelections () {
        
        //Did the user make a selection for this Poll Question
        if (questionSelected.user_selection > 0) {
                
            let selection_index = returnPollSelectionIndex(select: questionSelected.user_selection, optionsArray: questionSelected.options)
            
            if (selection_index > -1) {
                //Index of the selection that was made (Casted to String)
                userQuestionsAnsweredSelected.optionSelected = String(selection_index)
            } else {
                userQuestionsAnsweredSelected.optionSelected = ""
            }
                

            //Did the user like this Poll Question?
            userQuestionsAnsweredSelected.liked = questionSelected.user_liked
        }
    }
    
    /**
     * This return the index number of the Selected Choice from the arrray of Choices of a particular Poll
     *  - We will also be setting the number of people who voted this poll here
     */
    func returnPollSelectionIndex (select: Int, optionsArray: [Any])->Int {
        
        for i in 0...optionsArray.count {

            let optionsObject = optionsArray[i] as? [String: Any]
            
            //ID of the choice
            let choiceID = optionsObject?["id"] as? Int
            
            if (select == choiceID) {
                 print("Selection Index : ", i)
                 return i
            }
        }
        
        //Cannot find Choice ID, so we return an error
        return -1
    }
    
    //
    
    @objc func shareButtonPresed(sender: UIButton) {
        let text = "Question: \(self.questionSelected.title ?? "")\n\nOption 1: \(self.questionSelected.option1 ?? "")\n\nOption 2: \(self.questionSelected.option2 ?? "")\n\nOption 3: \(self.questionSelected.option3 ?? "")"
        let textToShare = [ text ]
        let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
      //  activityViewController.popoverPresentationController?.sourceView = self.view
        activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
          if let popoverController = activityViewController.popoverPresentationController {
                      popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                      popoverController.sourceView = self.view
                      popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
                  }

                  self.present(activityViewController, animated: true, completion: nil)
    }
    
    @objc func btnDeleteCommentPresed(sender: UIButton) {
                
          let alert = UIAlertController(title: "Delete Comment", message: "Are you sure you want to delete this comment?", preferredStyle: UIAlertController.Style.alert)

          alert.addAction(UIAlertAction(title: "No", style: UIAlertAction.Style.default, handler: { _ in
                     //Cancel Action
                 }))
          alert.addAction(UIAlertAction(title: "Yes", style: UIAlertAction.Style.destructive, handler: {(_: UIAlertAction!) in
              DispatchQueue.main.async {
                 SVProgressHUD.show(withStatus: "Deleting Comment...")
              }
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : self.questionSelected.id!,
                              "commentID"  : self.commentsModel[sender.tag].id] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.REMOVE_COMMENT_URL)!

            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            DispatchQueue.main.async {
                print("Comment Index : ", sender.tag)
                
                //Remove from Array
                self.commentsModel.remove(at: sender.tag)
                
                //Change Count
                self.questionSelected.comments = self.commentsModel.count
                
                print("numbers of comment : " , self.commentsModel.count)
                
                SVProgressHUD.dismiss()
                Toast(text: "Your comment has been removed!").show()
                
                self.tblCommentView.reloadData()
            }

            
            /*let ref = Database.database().reference().child("Questions").child(self.questionSelected.id).child("AllComments").child(Auth.auth().currentUser?.uid ?? "").child(self.commentsModel[sender.tag].strTimeStamp)
              ref.removeValue { error, _ in
                  if error == nil {
                      DispatchQueue.main.async {
                         // self.tableView.reloadData()
                          SVProgressHUD.dismiss()
                          Toast(text: "Your comment has been removed!").show()
                        let addReference2 = Database.database().reference().child("Questions").child(self.questionSelected.id)
                        addReference2.child("comments").setValue(self.commentsModel.count)
                      }
                  }
                  else {
                      SVProgressHUD.dismiss()
                      Toast(text: "Unable to comment Question!!!").show()
                  }
              }*/
            
            }))
          self.present(alert, animated: true, completion: nil)
      }
    
    
    @objc func option1ButtonPresed(sender: UIButton) {
        
        //If the user just clicked on themselves
        if self.userQuestionsAnsweredSelected.optionSelected == "0" {
            return
        }
        
        else {
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : self.questionSelected.id!,
                              "selection"  : self.questionSelected.option1!] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.SET_POLL_SELECTION)!

            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            //Has the user Voted in this Poll yet?
            if (Int(self.userQuestionsAnsweredSelected.optionSelected)! < 0) {
                
                //Increment the Vote because the user has NOT voted on this Poll yet
                self.numVoted = self.numVoted + 1
            } else {
                
                //Remove the old Selection
                let previous_selection = Int(self.userQuestionsAnsweredSelected.optionSelected)
                
                //Change the num_voted variable in the choices array
                var optionsObject = self.questionSelected!.options[previous_selection!] as? [String: Any]
                
                //Set the Vote Counter
                optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int - 1
                
                //reassign
                self.questionSelected!.options[previous_selection!] = optionsObject!
            }
        
            //Get the First Option Array
            var optionsObject = self.questionSelected!.options[0] as? [String: Any]
            
            //Set the Vote Counter
            optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int + 1
            
            //Reassign
            self.questionSelected!.options[0] = optionsObject!
            
            self.userQuestionsAnsweredSelected.optionSelected = "0"
            
            self.questionSelected.user_selection = (optionsObject!["id"] as! Int)
            
            //Reload the Table to show the changes
            DispatchQueue.main.async {
                self.tblCommentView.reloadData()
            }
        }
        
    }
    
    @objc func option2ButtonPresed(sender: UIButton) {
        
        //If the user just clicked on themselves
        if self.userQuestionsAnsweredSelected.optionSelected == "1" {
            return
        }
        
        else {
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : self.questionSelected.id!,
                              "selection"  : self.questionSelected.option2!] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.SET_POLL_SELECTION)!

            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            //Has the user Voted in this Poll yet?
            if (Int(self.userQuestionsAnsweredSelected.optionSelected)! < 0) {
                
                //Increment the Vote because the user has NOT voted on this Poll yet
                self.numVoted = self.numVoted + 1
            } else {
                
                //Remove the old Selection
                let previous_selection = Int(self.userQuestionsAnsweredSelected.optionSelected)
                
                //Change the num_voted variable in the choices array
                var optionsObject = self.questionSelected!.options[previous_selection!] as? [String: Any]
                
                //Set the Vote Counter
                optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int - 1
                
                //reassign
                self.questionSelected!.options[previous_selection!] = optionsObject!
            }
            
          
            //Get the First Option Array
            var optionsObject = self.questionSelected!.options[1] as? [String: Any]
            
            //Set the Vote Counter
            optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int + 1
            
            //Reassign
            self.questionSelected!.options[1] = optionsObject!
            
            self.userQuestionsAnsweredSelected.optionSelected = "1"
        
            self.questionSelected.user_selection = (optionsObject!["id"] as! Int)

            //Reload the Table to show the changes
            DispatchQueue.main.async {
                self.tblCommentView.reloadData()
            }
        }
        
    }
    @objc func option3ButtonPresed(sender: UIButton) {
        
        //If the user just clicked on themselves
        if self.userQuestionsAnsweredSelected.optionSelected == "2" {
            return
        }
        
        else {
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : self.questionSelected.id!,
                              "selection"  : self.questionSelected.option3!] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.SET_POLL_SELECTION)!

            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
                        
            //Has the user Voted in this Poll yet?
            if (Int(self.userQuestionsAnsweredSelected.optionSelected)! < 0) {
                
                //Increment the Vote because the user has NOT voted on this Poll yet
                self.numVoted = self.numVoted + 1
            } else {
                
                //Remove the old Selection
                let previous_selection = Int(self.userQuestionsAnsweredSelected.optionSelected)
                
                //Change the num_voted variable in the choices array
                var optionsObject = self.questionSelected!.options[previous_selection!] as? [String: Any]
                
                //Set the Vote Counter
                optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int - 1
                
                //reassign
                self.questionSelected!.options[previous_selection!] = optionsObject!
            }
            
          
            //Get the First Option Array
            var optionsObject = self.questionSelected!.options[2] as? [String: Any]
            
            //Set the Vote Counter
            optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int + 1
            
            //Reassign
            self.questionSelected!.options[2] = optionsObject!
            
            self.userQuestionsAnsweredSelected.optionSelected = "2"
        
            self.questionSelected.user_selection = (optionsObject!["id"] as! Int)

            //Reload the Table to show the changes
            DispatchQueue.main.async {
                self.tblCommentView.reloadData()
            }
        }
        
        
    }
    @objc func option4ButtonPresed(sender: UIButton) {
        
        //If the user just clicked on themselves
        if self.userQuestionsAnsweredSelected.optionSelected == "3" {
            return
        }
        
        else {
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : self.questionSelected.id!,
                              "selection"  : self.questionSelected.option4!] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.SET_POLL_SELECTION)!
            
            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            //Has the user Voted in this Poll yet?
            if (Int(self.userQuestionsAnsweredSelected.optionSelected)! < 0) {
                
                //Increment the Vote because the user has NOT voted on this Poll yet
                self.numVoted = self.numVoted + 1
            } else {
                
                //Remove the old Selection
                let previous_selection = Int(self.userQuestionsAnsweredSelected.optionSelected)
                
                //Change the num_voted variable in the choices array
                var optionsObject = self.questionSelected!.options[previous_selection!] as? [String: Any]
                
                //Set the Vote Counter
                optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int - 1
                
                //reassign
                self.questionSelected!.options[previous_selection!] = optionsObject!
            }
            
          
            //Get the First Option Array
            var optionsObject = self.questionSelected!.options[3] as? [String: Any]
            
            //Set the Vote Counter
            optionsObject!["num_votes"] = optionsObject?["num_votes"] as! Int + 1
            
            //Reassign
            self.questionSelected!.options[3] = optionsObject!
            
            self.userQuestionsAnsweredSelected.optionSelected = "3"
        
            self.questionSelected.user_selection = (optionsObject!["id"] as! Int)

            //Reload the Table to show the changes
            DispatchQueue.main.async {
                self.tblCommentView.reloadData()
            }
        }
        
    }
    
    /***
     * When the Like Button is pressed
     */
    @objc func likesButtonPresed(sender: UIButton) {
        
        var numberOfLikes: Int = 0
        
        var url: URL
        
        //Set and unset Liked Statused on UI
        if (self.userQuestionsAnsweredSelected.liked) {
            self.userQuestionsAnsweredSelected.liked = false
            numberOfLikes = self.questionSelected.likes - 1
            self.questionSelected.likes = numberOfLikes
            
            //Set URL to add a like
            url = URL(string: Constants.REMOVE_LIKE_URL)!
            
        }
        else {
            self.userQuestionsAnsweredSelected.liked = true
            numberOfLikes = self.questionSelected.likes + 1
            self.questionSelected.likes = numberOfLikes
            
            //Set URL to Remove a like
            url = URL(string: Constants.ADD_LIKE_URL)!
        }
                
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : questionSelected.id] as [String : Any]
    

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Reload the Table to show the changes
        DispatchQueue.main.async {
            self.tblCommentView.reloadData()
        }
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
        //NotificationCenter.default.post(name: .updateDashboard, object: nil)
    }
    
    @IBAction func saveCommentPressed(_ sender: Any) {
        if txtMessageView.text?.isEmpty ?? true {
            Toast(text: "Please enter comment!!!").show()
        }
        else {
            //self.txtMessageView.resignFirstResponder()
          //  self.view.endEditing(true)
                    
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "pollID"     : questionSelected.id!,
                              "comment"    : self.txtMessageView.text!] as [String : Any]
        

            let url = URL(string: Constants.ADD_COMMENT_URL)!
            
            //create the session object
            let session = URLSession.shared

            //now create the URLRequest object using the url object
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")

            let comment = self.txtMessageView.text
            
            do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                    } catch let error {
                                print(error.localizedDescription)
                    }
                                    
                    //create dataTask using the session object to send data to the server
                    let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                        guard error == nil else {
                                return
                        }

                        guard let data = data else {
                                return
                        }

                        print(response)
                                        
                        do {
                            //create json object from data
                            if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                            
                                //Get the return Code of the HTTP Request
                                let code = json["code"] as? Int;
                                                    
                                //Successfully able to retrieve user from Database
                                if (code == 1) {
                                    let data = json["data"] as! [String:Any];
                                                        
                                    //Set User Values
                                    let commentID = data["id"] as! Int
                                    let username  = data["username"] as! String
                                    let userID    = UserDefaults.standard.integer(forKey: "userID")
                                    let timeStamp = data["updated_at"] as? String
                                    
                                    //Set User Values
                                    let newCommentModel: CommentsModel = CommentsModel(id: commentID, name: username, comment: comment ?? "", timeStamp: timeStamp! , userID: String(userID), strTimeStamp: timeStamp! )

                                    self.commentsModel.append(newCommentModel)
                                    
                                    self.questionSelected.comments = self.commentsModel.count
                                    
                                    //Reload the Table to show the changes
                                    DispatchQueue.main.async {
                                        self.tblCommentView.reloadData()
                                    }
                                }
                                                
                             }
                         } catch let error {
                                print(error.localizedDescription)
                        }
                      })
                                    
            task.resume()
                          
            //Reset Message
            self.txtMessageView.text = ""
        }
    }
    
}


extension CommentViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            return 1
        } else {
            return self.commentsModel.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
        
        if indexPath.section == 0 {
            
            tableView.register(UINib(nibName: String(describing: QuestionTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: QuestionTableViewCell.self))
                
            let cell : QuestionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: String(describing: QuestionTableViewCell.self)) as? QuestionTableViewCell
                        
            let thisQuestionModel = self.questionSelected
                
            //Other Variables
            cell?.titleTextView.text = self.questionSelected.title
                            
            //Set the Selection
            let selectedChoiceIndex = Int(self.userQuestionsAnsweredSelected.optionSelected)
                
            //Hide the thank you label until a selection has been chosen
            cell.lblThankyou.isHidden = true;
            
            //Dynamically creating the UI Objects
            for i in 0...thisQuestionModel!.options.count - 1 {
                                
                //Get the Object from the Array
                let optionsObject = thisQuestionModel!.options[i] as? [String: Any]
                   
                //First Choice
                if (i == 0) {
                    
                    //Set the Text of the Label
                    cell.lblOption1.text = optionsObject!["content"] as? String
                    
                    //Calculate the percentage of Voters whom selected this Option
                    let numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                    var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted) * 100
                                    
                    //Set value to 0 is it is undefined for asthetic reasons
                    if (value.isNaN) {
                        value = 0
                    }
                
                    cell.lblPercentageOption1.text = String(format: "%.1f", value) + " %"
                    cell.lblPercentageOption1.textColor = self.hexStringToUIColor(hex: "#0B090C")
                    cell.lblPercentageOption1.textAlignment = .right
                    
                    //Set the Progress Bar
                    cell.pgOption1.layer.cornerRadius = cell.pgOption1.frame.height / 2
                    cell.pgOption1.clipsToBounds = true
                    cell.pgOption1.layer.masksToBounds = true
                    cell.pgOption1.setProgress(Float(value / 100), animated: true)
                    
                    //Register the Button
                    cell.option1Button.layer.cornerRadius = cell.option1Button.frame.height / 2
                    cell.option1Button.clipsToBounds = true
                    cell.option1Button.layer.masksToBounds = true
                
                    //Show the Tint
                    cell.pgOption1.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")
                    
                    //Set the asthetics for if this option as selected
                    if (selectedChoiceIndex == i) {
                        cell.imgOptionSelect1.isHidden = false;
                        cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                        cell.option1Button.layer.borderWidth = 2
                        cell.lblThankyou.isHidden = false;
                        
                    } else {
                        cell.imgOptionSelect1.isHidden = true;
                        cell.option1Button.layer.borderWidth = 1
                    }
                    
                    
                }
                //Second Choice
                else if (i == 1) {
                    
                    //Set the Text of the Label
                    cell.lblOption2.text = optionsObject!["content"] as? String
                    
                    //Calculate the percentage of Voters whom selected this Option
                    let numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                    var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted) * 100
                                    
                    //Set value to 0 is it is undefined for asthetic reasons
                    if (value.isNaN) {
                        value = 0
                    }
                
                    cell.lblPercentageOption2.text = String(format: "%.1f", value) + " %"
                    cell.lblPercentageOption2.textColor = self.hexStringToUIColor(hex: "#0B090C")
                    cell.lblPercentageOption2.textAlignment = .right
                    
                    //Set the Progress Bar
                    cell.pgOption2.layer.cornerRadius = cell.pgOption2.frame.height / 2
                    cell.pgOption2.clipsToBounds = true
                    cell.pgOption2.layer.masksToBounds = true
                    cell.pgOption2.setProgress(Float(value / 100), animated: true)
                    
                    //Register the Button
                    cell.option2Button.layer.cornerRadius = cell.option2Button.frame.height / 2
                    cell.option2Button.clipsToBounds = true
                    cell.option2Button.layer.masksToBounds = true

                    //Tint Color
                    cell.pgOption2.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                    //Set the asthetics for if this option as selected
                    if (selectedChoiceIndex == i) {
                        cell.imgOptionSelect2.isHidden = false;
                        cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                        cell.option2Button.layer.borderWidth = 2
                        cell.lblThankyou.isHidden = false
                    } else {
                        cell.imgOptionSelect2.isHidden = true;
                        cell.option2Button.layer.borderWidth = 1
                    }
                }
                //Third Choice
                else if (i == 2) {
                    
                    //Set the Text of the Label
                    cell.lblOption3.text = optionsObject!["content"] as? String
                    
                    //Calculate the percentage of Voters whom selected this Option
                    let numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                    var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted) * 100
                                    
                    //Set value to 0 is it is undefined for asthetic reasons
                    if (value.isNaN) {
                        value = 0
                    }
                
                    cell.lblPercentageOption3.text = String(format: "%.1f", value) + " %"
                    cell.lblPercentageOption3.textColor = self.hexStringToUIColor(hex: "#0B090C")
                    cell.lblPercentageOption3.textAlignment = .right
                    
                    //Set the Progress Bar
                    cell.pgOption3.layer.cornerRadius = cell.pgOption3.frame.height / 2
                    cell.pgOption3.clipsToBounds = true
                    cell.pgOption3.layer.masksToBounds = true
                    cell.pgOption3.setProgress(Float(value / 100), animated: true)
                    
                    //Register the Button
                    cell.option3Button.layer.cornerRadius = cell.option3Button.frame.height / 2
                    cell.option3Button.clipsToBounds = true
                    cell.option3Button.layer.masksToBounds = true

                    //Tint Color
                    cell.pgOption3.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                    //Set the asthetics for if this option as selected
                    if (selectedChoiceIndex == i) {
                        cell.imgOptionSelect3.isHidden = false;
                        cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                        cell.option3Button.layer.borderWidth = 2
                        cell.lblThankyou.isHidden = false;

                    } else {
                        cell.imgOptionSelect3.isHidden = true;
                        cell.option3Button.layer.borderWidth = 1
                    }
                }
                //Last Choice
                else if (i == 3) {
                    
                    //Set the Text of the Label
                    cell.lblOption4.text = optionsObject!["content"] as? String
                    
                    //Calculate the percentage of Voters whom selected this Option
                    let numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                    var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted) * 100
                                    
                    //Set value to 0 is it is undefined for asthetic reasons
                    if (value.isNaN) {
                        value = 0
                    }
                
                    cell.lblPercentageOption4.text = String(format: "%.1f", value) + " %"
                    cell.lblPercentageOption4.textColor = self.hexStringToUIColor(hex: "#0B090C")
                    cell.lblPercentageOption4.textAlignment = .right
                    
                    //Set the Progress Bar
                    cell.pgOption4.layer.cornerRadius = cell.pgOption4.frame.height / 2
                    cell.pgOption4.clipsToBounds = true
                    cell.pgOption4.layer.masksToBounds = true
                    cell.pgOption4.setProgress(Float(value / 100), animated: true)
                    
                    //Register the Button
                    cell.option4Button.layer.cornerRadius = cell.option4Button.frame.height / 2
                    cell.option4Button.clipsToBounds = true
                    cell.option4Button.layer.masksToBounds = true

                    //Tint Color
                    cell.pgOption4.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                    //Set the asthetics for if this option as selected
                    if (selectedChoiceIndex == i) {
                        cell.imgOptionSelect4.isHidden = false;
                        cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                        cell.option4Button.layer.borderWidth = 2
                        cell.isHidden = false
                    } else {
                        cell.imgOptionSelect4.isHidden = true;
                        cell.option4Button.layer.borderWidth = 1
                    }
                }
            }
            
            cell.likesLabel.text = String(thisQuestionModel!.likes)
            cell.commentsLabel.text = String(thisQuestionModel!.comments)
            
            //Fil in the Heart if this current user had liked the Poll
            if self.userQuestionsAnsweredSelected.liked {
                    cell?.likeImageView.image = UIImage(named: "heart_filled_teal")
            }
            else {
                    cell?.likeImageView.image = UIImage(named: "heart")
            }
            
            //Used to identify which poll we were working with
            cell?.likesButton.tag = indexPath.row
            cell?.commentsButton.tag = indexPath.row
            cell?.shareButton.tag = indexPath.row
            
            cell?.option1Button.tag = indexPath.row
            cell?.option2Button.tag = indexPath.row
            cell?.option3Button.tag = indexPath.row
            cell?.option4Button.tag = indexPath.row
            
            //Add Button Listeners
            cell?.option1Button.addTarget(self, action: #selector(option1ButtonPresed(sender:)), for: .touchUpInside)
            cell?.option2Button.addTarget(self, action: #selector(option2ButtonPresed(sender:)), for: .touchUpInside)
            cell?.option3Button.addTarget(self, action: #selector(option3ButtonPresed(sender:)), for: .touchUpInside)
            cell?.option4Button.addTarget(self, action: #selector(option4ButtonPresed(sender:)), for: .touchUpInside)
            cell?.likesButton.addTarget(self, action: #selector(likesButtonPresed(sender:)), for: .touchUpInside)
            cell?.shareButton.addTarget(self, action: #selector(shareButtonPresed(sender:)), for: .touchUpInside)
            
            return cell
            
        } else {
            
            if (Int(self.commentsModel[indexPath.row].userID!) == UserDefaults.standard.integer(forKey: "userID")) {
                tableView.register(UINib(nibName: String(describing: UserCommentCell.self), bundle: nil), forCellReuseIdentifier: String(describing: UserCommentCell.self))
                
                let cell : UserCommentCell! = tableView.dequeueReusableCell(withIdentifier: String(describing: UserCommentCell.self)) as? UserCommentCell
                cell?.nameLabel.text = self.commentsModel[indexPath.row].name
                cell?.lblComment.text = self.commentsModel[indexPath.row].comment
                
                cell.btnDelete.isHidden = false
                cell.btnDelete.tag = indexPath.row
                cell?.btnDelete.addTarget(self, action: #selector(btnDeleteCommentPresed(sender:)), for: .touchUpInside)
                
                return cell
            } else {
                tableView.register(UINib(nibName: String(describing: CommentsCell.self), bundle: nil), forCellReuseIdentifier: String(describing: CommentsCell.self))
                
                let cell : CommentsCell! = tableView.dequeueReusableCell(withIdentifier: String(describing: CommentsCell.self)) as? CommentsCell
                cell?.nameLabel.text = self.commentsModel[indexPath.row].name
                cell?.lblComment.text = self.commentsModel[indexPath.row].comment
                return cell
            }
            
            
        }
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    
}

