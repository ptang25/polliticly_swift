//
//  SettingViewController.swift
//  Polliticly
//
//  Created by Apple on 16/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import Toaster
import SVProgressHUD
import Firebase
import FirebaseAuth
import FirebaseMessaging
import iOSDropDown
import DatePickerDialog
import SideMenu
import SDWebImage
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class SettingViewController: UIViewController {

    @IBOutlet weak var settingView: UIView!
    
    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var genderTextField: DropDown!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var HouseNumTextField: UITextField!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var stateTextField: DropDown!
    
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var ethicity: DropDown!
    @IBOutlet weak var imageProfileView: UIImageView!
    
    //Labels
    @IBOutlet weak var settingLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var firstNameLabel: UILabel!
    @IBOutlet weak var lastNameLabel: UILabel!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dobLabel: UILabel!
    @IBOutlet weak var genderLabel: UILabel!
    @IBOutlet weak var ethnicityLabel: UILabel!
    @IBOutlet weak var zcLabel: UILabel!
    @IBOutlet weak var houseNumLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    
    
    var ref: DatabaseReference!
    var imageName = "\(UserDefaults.standard.string(forKey: "email") ?? "").jpg"
    var path = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                
                //Set Background Color
                self.settingView.backgroundColor = UIColor.black;
                
                //Set Label Colors
                self.settingLabel.textColor = UIColor.white;
                self.emailLabel.textColor = UIColor.white;
                self.firstNameLabel.textColor = UIColor.white;
                self.lastNameLabel.textColor = UIColor.white;
                self.usernameLabel.textColor = UIColor.white;
                self.dobLabel.textColor = UIColor.white;
                self.genderLabel.textColor = UIColor.white;
                self.ethnicityLabel.textColor = UIColor.white;
                self.houseNumLabel.textColor = UIColor.white;
                self.streetLabel.textColor = UIColor.white;
                self.zcLabel.textColor = UIColor.white;

                //Set Text Boxes
                self.userNameTextField.backgroundColor = UIColor.white
                self.userNameTextField.textColor = UIColor.darkGray
                self.userNameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.emailTextField.backgroundColor = UIColor.white
                self.emailTextField.textColor = UIColor.darkGray
                self.emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.zipCodeTextField.backgroundColor = UIColor.white
                self.zipCodeTextField.textColor = UIColor.darkGray
                self.zipCodeTextField.attributedPlaceholder = NSAttributedString(string: "Zip Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.HouseNumTextField.backgroundColor = UIColor.white
                self.HouseNumTextField.textColor = UIColor.darkGray
                self.HouseNumTextField.attributedPlaceholder = NSAttributedString(string: "House Number (Ex. 315)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.streetTextField.backgroundColor = UIColor.white
                self.streetTextField.textColor = UIColor.darkGray
                self.streetTextField.attributedPlaceholder = NSAttributedString(string: "Street (Ex. West 100)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.birthdayTextField.backgroundColor = UIColor.white
                self.birthdayTextField.textColor = UIColor.darkGray
                self.birthdayTextField.attributedPlaceholder = NSAttributedString(string: "Date of birth", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.lastNameTextField.backgroundColor = UIColor.white
                self.lastNameTextField.textColor = UIColor.darkGray
                self.lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.firstNameTextField.backgroundColor = UIColor.white
                self.firstNameTextField.textColor = UIColor.darkGray
                self.firstNameTextField.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.cityTextField.backgroundColor = UIColor.white
                self.cityTextField.textColor = UIColor.darkGray
                self.cityTextField.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.genderTextField.backgroundColor = UIColor.white
                self.genderTextField.textColor = UIColor.darkGray
                self.genderTextField.rowBackgroundColor = UIColor.darkGray
                self.genderTextField.attributedPlaceholder = NSAttributedString(string: "Gender", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.ethicity.backgroundColor = UIColor.white
                self.ethicity.textColor = UIColor.darkGray
                self.ethicity.rowBackgroundColor = UIColor.darkGray
                self.ethicity.attributedPlaceholder = NSAttributedString(string: "Ethnicity", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.stateTextField.backgroundColor = UIColor.white
                self.stateTextField.textColor = UIColor.darkGray
                self.stateTextField.rowBackgroundColor = UIColor.darkGray
                self.stateTextField.attributedPlaceholder = NSAttributedString(string: "Ethnicity", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                
            }
        }
        // Do any additional setup after loading the view.
        setupView()
        getProfileData()
        path = "profile/image\(imageName)"
        
    }
    
    

    
    @IBAction func wednesdayTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func editTapped(_ sender: UIButton) {
        updateProfile()
    }
    
    
   @IBAction func profileTapped(_ sender: UIButton) {
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.imageProfileView.image = image
        }
    }
    
    func setupView(){
           emailTextField.setLeftPaddingPoints(8)
           emailTextField.setRightPaddingPoints(8)
           firstNameTextField.setLeftPaddingPoints(8)
           lastNameTextField.setRightPaddingPoints(8)
           lastNameTextField.setLeftPaddingPoints(8)
           firstNameTextField.setRightPaddingPoints(8)
           birthdayTextField.setLeftPaddingPoints(8)
           genderTextField.setRightPaddingPoints(8)
           genderTextField.setLeftPaddingPoints(8)
           birthdayTextField.setRightPaddingPoints(8)
           zipCodeTextField.setRightPaddingPoints(8)
           zipCodeTextField.setLeftPaddingPoints(8)
           userNameTextField.setRightPaddingPoints(8)
           userNameTextField.setLeftPaddingPoints(8)
           cityTextField.setRightPaddingPoints(8)
           cityTextField.setLeftPaddingPoints(8)
           stateTextField.setRightPaddingPoints(8)
           stateTextField.setLeftPaddingPoints(8)
        
        ethicity.setRightPaddingPoints(8)
        ethicity.setLeftPaddingPoints(8)
           genderTextField.optionArray = ["Male", "Female", "Non-Binary", "Other", "Prefer Not to Say"]
          // genderTextField.arrowColor = self.hexStringToUIColor(hex: "#7E3BF2")
           genderTextField.didSelect{(selectedText , index ,id) in
               self.genderTextField.text = selectedText
           }
           
           ethicity.optionArray = ["Asian / Pacific Islander", "Black or African American", "Hispanic or Latino", "Native American or American Indian", "White", "Other", "Prefer Not to Say"]
        //   ethicity.arrowColor = self.hexStringToUIColor(hex: "#7E3BF2")
           //spokenGenderTextField.rowBackgroundColor = UIColor.white
           ethicity.didSelect{(selectedText , index ,id) in
               self.ethicity.text = selectedText
           }
        
        stateTextField.optionArray = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        stateTextField.arrowColor = self.hexStringToUIColor(hex: "#1FD8B9")
        stateTextField.didSelect{(selectedText , index ,id) in
                self.stateTextField.text = selectedText
        }
        
        SettingViewController.loadImage(imageView: imageProfileView, urlString: UserDefaults.standard.string(forKey: image) ?? "", placeHolderImageString: "img_placeholder")
       }

       
       @IBAction func birthdayTextFieldEditingBegin(_ sender: Any) {
           DatePickerDialog().show("Select Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
               (date) -> Void in
               if let dt = date {
                   let formatter = DateFormatter()
                   formatter.dateFormat = "MM/dd/yyyy"
                   self.birthdayTextField.text = formatter.string(from: dt)
               }
           }
           self.view.endEditing(true)
       }
       
       
       func hexStringToUIColor (hex:String) -> UIColor {
           var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

           if (cString.hasPrefix("#")) {
               cString.remove(at: cString.startIndex)
           }

           if ((cString.count) != 6) {
               return UIColor.gray
           }

           var rgbValue:UInt64 = 0
           Scanner(string: cString).scanHexInt64(&rgbValue)

           return UIColor(
               red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
               green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
               blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
               alpha: CGFloat(1.0)
           )
       }
    
    func getProfileData(){
        /*ref = Database.database().reference(withPath: "Users").child(Auth.auth().currentUser?.uid ?? "")
                ref.observeSingleEvent(of: .value, with: { snapshot in
                    if !snapshot.exists() { return }
                    for child in snapshot.children {
                        let childSnap = child as! DataSnapshot
                        if childSnap.key == "userName" {
                            self.userNameTextField.text = childSnap.value as? String ?? ""
                        }
                        if childSnap.key == "zipCode" {
                            self.zipCodeTextField.text = childSnap.value as? String ?? ""
                        }
                        
                        if childSnap.key == "email" {
                            self.emailTextField.text = childSnap.value as? String ?? ""
                        }
                        if childSnap.key == "gender" {
                            self.genderTextField.text = childSnap.value as? String ?? ""
                        }
                        
                        if childSnap.key == "ethnicity" {
                            self.ethicity.text = childSnap.value as? String ?? ""
                        }
                        
                        if childSnap.key == "birthday" {
                            self.birthdayTextField.text = childSnap.value as? String ?? ""
                        }
                        
                        if childSnap.key == "firstName" {
                            self.firstNameTextField.text = childSnap.value as? String ?? ""
                        }
                        
                        if childSnap.key == "lastName" {
                            self.lastNameTextField.text = childSnap.value as? String ?? ""
                        }
                    }
                })*/
            self.userNameTextField.text = UserDefaults.standard.string(forKey: "username") ?? ""
            self.zipCodeTextField.text = UserDefaults.standard.string(forKey: "zipCode") ?? ""
            self.emailTextField.text = UserDefaults.standard.string(forKey: "email") ?? ""
            self.genderTextField.text = UserDefaults.standard.string(forKey: "gender") ?? ""
            self.ethicity.text = UserDefaults.standard.string(forKey: "ethnicity") ?? ""
            self.birthdayTextField.text = UserDefaults.standard.string(forKey: "birthday") ?? ""
            self.firstNameTextField.text = UserDefaults.standard.string(forKey: "first_name") ?? ""
            self.lastNameTextField.text = UserDefaults.standard.string(forKey: "last_name") ?? ""
            self.HouseNumTextField.text = UserDefaults.standard.string(forKey: "house_num") ?? ""
            self.streetTextField.text = UserDefaults.standard.string(forKey: "street") ?? ""
            self.cityTextField.text = UserDefaults.standard.string(forKey: "city") ?? ""
            self.stateTextField.text = UserDefaults.standard.string(forKey: "state") ?? ""
    }
    
    class func loadImage(imageView: UIImageView, urlString: String, placeHolderImageString: String?)->Swift.Void{
        let placeHolder = UIImage(named: placeHolderImageString ?? "")
        imageView.image = placeHolder
        let encodedString:String = urlString.replacingOccurrences(of: " ", with: "%20")
        imageView.sd_imageIndicator = SDWebImageActivityIndicator.gray
        imageView.sd_setImage(with: URL(string: encodedString), placeholderImage: placeHolder)
        
        
    }
    
    func uploadImagePic(image: UIImage, name: String, filePath: String,completion: @escaping (_ url: String?) -> Void) {
        guard let imageData: Data = image.jpegData(compressionQuality: 0.1) else {
            return
        }

       

        let storageRef = Storage.storage().reference(withPath: filePath)

        
        storageRef.putData(imageData, metadata: nil) { (metadata, err) in
         storageRef.downloadURL { (url, error) in

            if error != nil {
                print("Failed to download url:", error!)
                 completion(nil)
                return
            }

            let imageUrl = "\(String(describing: url))"
            completion(url?.absoluteString)
                // postRef.child(autoID).setValue(imageUrl)
             }
        }
    }
    
    func updateProfile(){
        SVProgressHUD.show()
        self.uploadImagePic(image: imageProfileView.image!, name: self.imageName, filePath: path, completion: { url in
            
            /*UserDefaults.standard.set(url, forKey: image)
            let addReference = Database.database().reference().child("Users").child(Auth.auth().currentUser?.uid ?? "")
            
            addReference.child("firstName").setValue(self.firstNameTextField.text!)
            addReference.child("lastName").setValue(self.lastNameTextField.text!)
            addReference.child("email").setValue(self.emailTextField.text!)
            addReference.child("birthday").setValue(self.birthdayTextField.text!)
            addReference.child("gender").setValue(self.genderTextField.text!)
            addReference.child("zipCode").setValue(self.zipCodeTextField.text!)
            addReference.child("userName").setValue(self.userNameTextField.text!)
            addReference.child("ethnicity").setValue(self.ethicity.text!)*/
            
            //Save Changes to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "email"      : self.emailTextField.text!,
                              "first_name" : self.firstNameTextField.text!,
                              "last_name"  : self.lastNameTextField.text!,
                              "birthday"   : self.birthdayTextField.text!,
                              "gender"     : self.genderTextField.text!,
                              "zipCode"    : self.zipCodeTextField.text!,
                              "username"   : self.userNameTextField.text!,
                              "avatar"     : url!,
                              "street"     : self.streetTextField.text!,
                              "house_num"  : self.HouseNumTextField.text!,
                              "city"       : self.cityTextField.text!,
                              "state"      : self.stateTextField.text!,
                              "ethnicity"  : self.ethicity.text!] as [String : Any]
            
            //create the url with URL
            let url = URL(string: Constants.EDIT_VOTER_URL)!

            //create the session object
            let session = URLSession.shared

            //now create the URLRequest object using the url object
            var request = URLRequest(url: url)
            request.httpMethod = "POST" //set http method as POST
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")

            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
            } catch let error {
                    print(error.localizedDescription)
            }
            
            //create dataTask using the session object to send data to the server
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                guard error == nil else {
                    return
                }

                guard let data = data else {
                    return
                }

                print(response)
                
                do {
                    //create json object from data
                    if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                    
                        //Get the return Code of the HTTP Request
                        let code = json["code"] as? Int;
                            
                        //Successfully able to retrieve user from Database
                        if (code == 1) {
                            let data = json["data"] as! [String:Any];
                                
                            //Set User Values
                            UserDefaults.standard.setValue(data["id"], forKey: "userID")
                            UserDefaults.standard.setValue(data["first_name"], forKey: "first_name")
                            UserDefaults.standard.setValue(data["last_name"], forKey: "last_name")
                            UserDefaults.standard.setValue(data["username"], forKey: "username")
                            UserDefaults.standard.setValue(data["birthday"], forKey: "birthday")
                            UserDefaults.standard.setValue(data["ethnicity"], forKey: "ethnicity")
                            UserDefaults.standard.setValue(data["gender"], forKey: "gender")
                            UserDefaults.standard.setValue(data["avatar"], forKey: "avatar")
                            UserDefaults.standard.setValue(data["house_num"], forKey: "house_num")
                            UserDefaults.standard.setValue(data["zipCode"], forKey: "zipCode")
                            UserDefaults.standard.setValue(data["street"], forKey: "street")
                            UserDefaults.standard.setValue(data["city"], forKey: "city")
                            UserDefaults.standard.setValue(data["state"], forKey: "state")
                                
                        }
                        
                    }
                } catch let error {
                        print(error.localizedDescription)
                }
            })
            
            task.resume()
            
    
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                 Toast(text: "Profile updated successfully.").show()
               
                 //After changes are saved - we load the Main Screen
                 self.navigationController?.popViewController(animated: true)

            }
            
        })

    }
    
    

}
