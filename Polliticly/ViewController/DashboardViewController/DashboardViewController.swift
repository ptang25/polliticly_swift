//
//  DashboardViewController.swift
//  Polliticly
//
//  Created by Apple on 14/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import UIKit
import SVProgressHUD
import Firebase
import Toaster
import iOSDropDown
import SideMenu
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage
import FacebookShare
import PopupDialog

class DashboardViewController: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var contentView: UIView!
    var ref: DatabaseReference!
    var ref2: DatabaseReference!
    var ref3: DatabaseReference!
    var questionsModel = [QuestionsModel]()
    var userQuestionsAnsweredModel = [UserQuestionsAnsweredModel]()
    public static var questionSelected: QuestionsModel!
    public static var userQuestionsAnsweredSelected: UserQuestionsAnsweredModel!
    
    @IBOutlet weak var settingDropDown: DropDown!
    
    var myGender = ""
    var myZipCode = ""
    var myEthnicity = ""
    var myAge = 0
    var isAgeAllow = false
    var isZipCodeAllow = false
    var isEthnicityAllow = false
    var isGenderAllow = false
    var option1Count = 0
    var option2Count = 0
    var option3Count = 0
    var option4Count = 0
    public static var myName: String = ""
    var liked = [Bool]()
    var isAllowedToReload: Bool = false
    var arrQuestion = [QuestionsModel]()
    var arrAnswers = [UserQuestionsAnsweredModel]()
    
    //The Number of people who voted for each Poll
    var numVoted: [Int] = [Int]()
    
    //Refresh Control
    let refreshControl = UIRefreshControl()
    
    
    @IBOutlet weak var tableQuestionsView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
            //self.tableQuestionsView.estimatedRowHeight = 85
        
        self.tableQuestionsView.rowHeight = UITableView.automaticDimension
        self.tableQuestionsView.estimatedRowHeight = 150
        
        // Access Shared Defaults Object
        let userDefaults = UserDefaults.standard
        
        self.getData()
        setupView()
        setupSideMenu()
        
        
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)

        // this is the replacement of implementing: "collectionView.addSubview(refreshControl)"
        tableQuestionsView.refreshControl = refreshControl
        
        NotificationCenter.default.removeObserver(self, name: .updateDashboard, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.refresh(sender:)), name: .updateDashboard, object: nil)
        NotificationCenter.default.removeObserver(self, name: .clenDatabaseReference, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.clenDatabaseReference(sender:)), name: .clenDatabaseReference, object: nil)
    }
    
    
    @objc func refresh(sender:AnyObject) {
        self.getData()
                
        refreshControl.endRefreshing()
    }
    
    @objc func clenDatabaseReference(sender:AnyObject) {
        ref.removeAllObservers()
        ref2.removeAllObservers()
        ref3.removeAllObservers()
    }
    
    @IBAction func sideMenuTapped(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)
    }
    
    
    func getSelectedOptions(){
        let ref = Database.database().reference(withPath: "Users")
        ref.observeSingleEvent(of: .value, with: { snapshot in
            if !snapshot.exists() {
                print("empty")
                return
            }
            self.option1Count = 0
            self.option2Count = 0
            self.option3Count = 0
            self.option4Count = 0
            
            for snap in snapshot.children {
                let userSnap = snap as! DataSnapshot
                let userDict = userSnap.value as! [String:AnyObject] //child data
                if let answered = userDict["QuestionsAnswered"] as? [String:AnyObject]{
                    // print(answered)
                    
                    UserDefaults.standard.synchronize()
                    for item in answered{
                        let answer = item.value as! [String:AnyObject]
                        let obj = UserQuestionsAnsweredModel()
                        obj.question = answer["question"] as? String ?? ""
                        obj.seleted = answer["optionSelected"] as? String ?? ""
                        
                        self.arrAnswers.append(obj)
                        
                        let selectedAnswer = answer["optionSelected"] as? String ?? ""
                        if selectedAnswer == "option1"{
                            self.option1Count = self.option1Count + 1
                        }
                        else if selectedAnswer == "option2"{
                            self.option2Count = self.option2Count + 1
                        }
                        else if selectedAnswer == "option3"{
                            self.option3Count = self.option3Count + 1
                        }
                        else if selectedAnswer == "option4"{
                            self.option4Count = self.option4Count + 1
                        }
                    }
                    
                    
                }
                
            }
            for (_, element) in self.arrAnswers.enumerated() {
                for (_, element2) in self.questionsModel.enumerated() {
                    if element.question == element2.title{
                        if element.seleted == "option1"{
                            element2.optionSelected1 =  element2.optionSelected1 + 1
                        }
                        if element.seleted == "option2"{
                            element2.optionSelected2 =  element2.optionSelected2 + 1
                        }
                        if element.seleted == "option3"{
                            element2.optionSelected3 =  element2.optionSelected3 + 1
                        }
                        if element.seleted == "option4"{
                            element2.optionSelected4 =  element2.optionSelected4 + 1
                        }
                        
                    }
                }
            }
            
            /*(UserDefaults.standard.set(self.option1Count, forKey: "option1")
            UserDefaults.standard.set(self.option2Count, forKey: "option2")
            UserDefaults.standard.set(self.option3Count, forKey: "option3")
            UserDefaults.standard.set(self.option4Count, forKey: "option4")
            
            let totalAnswered = UserDefaults.standard.integer(forKey: "option1") + UserDefaults.standard.integer(forKey: "option2") + UserDefaults.standard.integer(forKey: "option3") + UserDefaults.standard.integer(forKey: "option4")
            UserDefaults.standard.set(totalAnswered, forKey: "totalAnswered")
            UserDefaults.standard.synchronize()*/
            DispatchQueue.main.async {
                SVProgressHUD.dismiss()
                self.tableQuestionsView.reloadData()
            }
        })
    
    }
    
    func getData() {
        
        SVProgressHUD.show(withStatus: "Loading...")
        
        
        self.arrAnswers = []
        self.arrQuestion = []
        
        var imageName = "\(UserDefaults.standard.string(forKey: "email") ?? "").jpg"
        let path  = "profile/image\(imageName)"
        
        let storageRef = Storage.storage().reference(withPath: path)
        storageRef.downloadURL(completion: { (url: URL?, error: Error?) in
            // print(url?.absoluteString) // <- Your url
            UserDefaults.standard.set(url?.absoluteString, forKey: image)
        })
        
        getPollData()

    }
    
    /**'
     * This Function retrieves the Poll Information the Database
     */
    func getPollData() {
             
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID")] as [String : Any]
        
        //create the url with URL
        let url = URL(string: Constants.GET_POLLS)!

        //create the session object
        let session = URLSession.shared

        //now create the URLRequest object using the url object
        var request = URLRequest(url: url)
        request.httpMethod = "POST" //set http method as POST
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")

        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
        } catch let error {
                print(error.localizedDescription)
        }
        
        //create dataTask using the session object to send data to the server
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

            guard error == nil else {
                return
            }

            guard let data = data else {
                return
            }

            print("Response -- ", response)
            
            do {
                //create json object from data
                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any] {
                
                    //Get the return Code of the HTTP Request
                    let code = json["code"] as? Int;
                        

                    //Successfully able to retrieve user from Database
                    if (code == 1) {
                                              
                        print(type(of: json["data"]))

                        //Get the Data
                        let data = json["data"] as! [Any]
                                                                      
                        DashboardViewController.myName = UserDefaults.standard.string(forKey:"username") ?? ""
                        
                        self.questionsModel = [QuestionsModel]()
                        
                        for poll in data  {
                           
                            //Cast it Properly
                            let myPoll = poll as? [String: Any]
                            
                            let pollID = String(myPoll?["id"] as? Int ?? 0)
                            let question = myPoll?["question"] as? String
                            
                            let sponsorTitle = myPoll?["sponsor"] as? String ?? ""
                            let learnMore = myPoll?["more"] as? String ?? ""
                            let ext_link = myPoll?["link"] as? String ?? ""
                            
                            let num_likes = myPoll?["num_likes"] as? Int ?? 0
                            let num_comments = myPoll?["num_comments"] as? Int ?? 0
                            let user_liked = myPoll?["user_liked"] as? Bool ?? false
                            
                            let poll_likes = myPoll?["poll_likes"] as! [Any]
                            let poll_choices = myPoll?["poll_choices"] as! [Any]
                            let poll_comments = myPoll?["poll_comments"] as! [Any]
                            
                            let user_selection = myPoll?["user_selection"] as! Int
                            
                            self.questionsModel.append(QuestionsModel(id: String(pollID), title: question, sponsorTitle: sponsorTitle, learnMore: learnMore, link: ext_link, likes: num_likes, comments: num_comments, options: poll_choices, poll_likes: poll_likes, poll_comments: poll_comments, user_liked: user_liked, user_comments: poll_comments, user_selection: user_selection))
                        
                            
                            //Find out if Voter like this Poll
                            //let user_likes = myPoll?["user_liked"] as? Bool ?? false
        
                            self.userQuestionsAnsweredModel.append(UserQuestionsAnsweredModel(id: String(pollID), liked: user_liked, optionSelected: String(user_selection)))

                        }
                         
                        //Store details about this current user's voted selections, and if they liked the poll
                        self.assignUserSelections()
                        
                        //Store the number of Voters per Poll
                        self.setNumberOfVotes()
                                                
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            self.tableQuestionsView.reloadData()
                        }
                    }
                    
                }
            } catch let error {
                    print(error.localizedDescription)
            }
        })
        
        task.resume()
        
        
        
    }

    /**
     * This Function assigns with selections a user has made on each individual Question
     */
    func assignUserSelections () {
    
        //Poll through the Questions
        for (index, qModel) in self.questionsModel.enumerated()  {
        
            //Did the user make a selection for this Poll Question
            if (qModel.user_selection > 0) {
                
                let selection_index = returnPollSelectionIndex(select: qModel.user_selection, optionsArray: qModel.options)
                
                if (selection_index > -1) {
                    //Index of the selection that was made (Casted to String)
                    self.userQuestionsAnsweredModel[index].optionSelected = String(selection_index)
                } else {
                    self.userQuestionsAnsweredModel[index].optionSelected = ""
                }
                
    
                //Did the user like this Poll Question?
                self.userQuestionsAnsweredModel[index].liked = qModel.user_liked
            }
        }
        
    }
    
    /**
     * This return the index number of the Selected Choice from the arrray of Choices of a particular Poll
     *  - We will also be setting the number of people who voted this poll here
     */
    func returnPollSelectionIndex (select: Int, optionsArray: [Any])->Int {
        
        for i in 0...optionsArray.count - 1 {

            let optionsObject = optionsArray[i] as? [String: Any]
            
            //ID of the choice
            let choiceID = optionsObject?["id"] as? Int
            
            if (select == choiceID) {
                 print("Selection Index : ", i)
                 return i
            }
        }
        
        //Cannot find Choice ID, so we return an error
        return -1
    }
    
    /**
     * Set the number of people who voted for each Poll
     */
    func setNumberOfVotes () {
        
        //Poll through the Questions
        for (index, qModel) in self.questionsModel.enumerated()  {
            
            var numVoters: Int = 0;
            
            for options in qModel.options {
                
                let optionsObject = options as? [String: Any]

                //Add the number of people who voted for this Poll together
                numVoters += optionsObject?["num_votes"] as? Int ?? 0
            }
                
            //Set the number into our Global variable based on Index variable which is the Question #
            self.numVoted.append(numVoters);
        }
    }
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
    
    func getAgeFromDOF(date: String) -> Int {
        
        let dateFormater = DateFormatter()
        dateFormater.dateFormat = "mm/dd/yyyy"
        let dateOfBirth = dateFormater.date(from: date)
        
        let calender = Calendar.current
        
        let dateComponent = calender.dateComponents([.year, .month, .day], from:
            dateOfBirth!, to: Date())
        
        return dateComponent.year ?? 0
    }
    
    func setupView() {
            contentView.layer.cornerRadius = 24
            contentView.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
            
            //Set Dark Mode Colors if in Dark Mode
            if #available(iOS 13.0, *) {
                if UITraitCollection.current.userInterfaceStyle == .dark {
                    //Change the Background Color of the Frame
                    contentView.backgroundColor = UIColor.black;
                }
            }
            
            settingDropDown.optionArray = ["Setting", "Logout"]
            settingDropDown.arrowColor = self.hexStringToUIColor(hex: "#1FD8B9")
            //spokenGenderTextField.rowBackgroundColor = UIColor.white
            settingDropDown.didSelect{(selectedText , index ,id) in
                if index == 0{
                    let viewController = SettingViewController(nibName: String(describing: SettingViewController.self), bundle: nil)
                    self.navigationController?.pushViewController(viewController, animated: true)
                }
                else{
                    
                }
            }
            
    }
    
    // MARK: - TableView Delegate
    func numberOfSectionsInTableView(tableView: UITableView) -> Int
    {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return self.questionsModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                        
        tableView.register(UINib(nibName: String(describing: QuestionTableViewCell.self), bundle: nil), forCellReuseIdentifier: String(describing: QuestionTableViewCell.self))
            
        let cell : QuestionTableViewCell! = tableView.dequeueReusableCell(withIdentifier: String(describing: QuestionTableViewCell.self)) as? QuestionTableViewCell
                    
        let thisQuestionModel = self.questionsModel[indexPath.row]
            

        //Other Variables
        cell.titleTextView.text = thisQuestionModel.title
        cell.sponsorLabel.text = thisQuestionModel.sponsorTitle
        /*cell.likesLabel.text = String(thisQuestionModel.likes)
        cell.commentsLabel.text = String(thisQuestionModel.comments)*/
                        
        //Set the Selection
        let selectedChoiceIndex = Int(self.userQuestionsAnsweredModel[indexPath.row].optionSelected)
            
        //Hide the Thank You note, reveal it only if the user has made a selection
        cell.lblThankyou.isHidden = true;
        
        //Dynamically creating the UI Objects
        for i in 0...thisQuestionModel.options.count - 1 {
                            
            //Get the Object from the Array
            let optionsObject = thisQuestionModel.options[i] as? [String: Any]
               
            //First Choice
            if (i == 0) {
                
                //Set the Text of the Label
                cell.lblOption1.text = optionsObject!["content"] as? String
                
                //Calculate the percentage of Voters whom selected this Option
                var numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                
                if (numVoted_currentChoice < 0) {
                    numVoted_currentChoice = 0;
                }
                
                var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted[indexPath.row]) * 100
                                
                //Set value to 0 is it is undefined for asthetic reasons
                if ( (value.isNaN) || (selectedChoiceIndex ?? -1 < 0) ){
                    value = 0
                }
                //Make it positive, if value ie negative
                else if (value < 0) {
                    value = value * -1;
                }
            
                print("Option 1 - TotalVotes ", self.numVoted[indexPath.row])
                print("Option 1 - NumVotes ", numVoted_currentChoice)
                print("Option 1 - Value ", value)
                
                cell.lblPercentageOption1.text = String(format: "%.1f", value) + " %"
                cell.lblPercentageOption1.textColor = self.hexStringToUIColor(hex: "#0B090C")
                cell.lblPercentageOption1.textAlignment = .right
                
                //Set the Progress Bar
                cell.pgOption1.layer.cornerRadius = cell.pgOption1.frame.height / 2
                cell.pgOption1.clipsToBounds = true
                cell.pgOption1.layer.masksToBounds = true
                cell.pgOption1.setProgress(Float(value / 100), animated: true)
                
                //Register the Button
                cell.option1Button.layer.cornerRadius = cell.option1Button.frame.height / 2
                cell.option1Button.clipsToBounds = true
                cell.option1Button.layer.masksToBounds = true
                
                //Tint Color
                cell.pgOption1.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                //Set the asthetics for if this option as selected
                if (selectedChoiceIndex == i) {
                    cell.imgOptionSelect1.isHidden = false;
                    cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                    cell.option1Button.layer.borderWidth = 2
                    cell.lblThankyou.isHidden = false;

                    //Disable being able to select same one again
                    cell.option1Button.isEnabled = false
                    cell.option2Button.isEnabled = true
                    cell.option3Button.isEnabled = true
                    cell.option4Button.isEnabled = true
                } else {
                    cell.imgOptionSelect1.isHidden = true;
                    cell.option1Button.layer.borderWidth = 1
                }
                
            }
            //Second Choice
            else if (i == 1) {
                
                //Set the Text of the Label
                cell.lblOption2.text = optionsObject!["content"] as? String
                
                //Calculate the percentage of Voters whom selected this Option
                var numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                
                if (numVoted_currentChoice < 0) {
                    numVoted_currentChoice = 0;
                }
                
                var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted[indexPath.row]) * 100
                                
                //Set value to 0 is it is undefined for asthetic reasons
                if ( (value.isNaN) || (selectedChoiceIndex ?? -1 < 0) ){
                    value = 0
                }
                //Make it positive, if value ie negative
                else if (value < 0) {
                    value = value * -1;
                }
            
                cell.lblPercentageOption2.text = String(format: "%.1f", value) + " %"
                cell.lblPercentageOption2.textColor = self.hexStringToUIColor(hex: "#0B090C")
                cell.lblPercentageOption2.textAlignment = .right
                
                //Set the Progress Bar
                cell.pgOption2.layer.cornerRadius = cell.pgOption2.frame.height / 2
                cell.pgOption2.clipsToBounds = true
                cell.pgOption2.layer.masksToBounds = true
                cell.pgOption2.setProgress(Float(value / 100), animated: true)
                
                //Register the Button
                cell.option2Button.layer.cornerRadius = cell.option2Button.frame.height / 2
                cell.option2Button.clipsToBounds = true
                cell.option2Button.layer.masksToBounds = true
                
                cell.pgOption2.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                //Set the asthetics for if this option as selected
                if (selectedChoiceIndex == i) {
                    cell.imgOptionSelect2.isHidden = false;
                    cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                    cell.option2Button.layer.borderWidth = 2
                    cell.lblThankyou.isHidden = false;
                    
                    //Disable being able to select same one again
                    cell.option1Button.isEnabled = true
                    cell.option2Button.isEnabled = false
                    cell.option3Button.isEnabled = true
                    cell.option4Button.isEnabled = true
                } else {
                    cell.imgOptionSelect2.isHidden = true;
                    cell.option2Button.layer.borderWidth = 1
                }
            }
            //Third Choice
            else if (i == 2) {
                
                //Set the Text of the Label
                cell.lblOption3.text = optionsObject!["content"] as? String
                
                //Calculate the percentage of Voters whom selected this Option
                var numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                
                if (numVoted_currentChoice < 0) {
                    numVoted_currentChoice = 0;
                }
                
                var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted[indexPath.row]) * 100
                                
                //Set value to 0 is it is undefined for asthetic reasons
                if ( (value.isNaN) || (selectedChoiceIndex ?? -1 < 0) ){
                    value = 0
                }
                //Make it positive, if value ie negative
                else if (value < 0) {
                    value = value * -1;
                }
            
                cell.lblPercentageOption3.text = String(format: "%.1f", value) + " %"
                cell.lblPercentageOption3.textColor = self.hexStringToUIColor(hex: "#0B090C")
                cell.lblPercentageOption3.textAlignment = .right
                
                //Set the Progress Bar
                cell.pgOption3.layer.cornerRadius = cell.pgOption3.frame.height / 2
                cell.pgOption3.clipsToBounds = true
                cell.pgOption3.layer.masksToBounds = true
                cell.pgOption3.setProgress(Float(value / 100), animated: true)
                
                //Register the Button
                cell.option3Button.layer.cornerRadius = cell.option3Button.frame.height / 2
                cell.option3Button.clipsToBounds = true
                cell.option3Button.layer.masksToBounds = true

                //Set the Tint
                cell.pgOption3.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")

                //Set the asthetics for if this option as selected
                if (selectedChoiceIndex == i) {
                    cell.imgOptionSelect3.isHidden = false;
                    cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                    cell.option3Button.layer.borderWidth = 2
                    cell.lblThankyou.isHidden = false;
                    
                    //Disable being able to select same one again
                    cell.option1Button.isEnabled = true
                    cell.option2Button.isEnabled = true
                    cell.option3Button.isEnabled = false
                    cell.option4Button.isEnabled = true

                } else {
                    cell.imgOptionSelect3.isHidden = true;
                    cell.option3Button.layer.borderWidth = 1
                }
            }
            //Last Choice
            else if (i == 3) {
                
                //Set the Text of the Label
                cell.lblOption4.text = optionsObject!["content"] as? String
                
                //Calculate the percentage of Voters whom selected this Option
                var numVoted_currentChoice = optionsObject?["num_votes"] as? Int ?? 0
                
                if (numVoted_currentChoice < 0) {
                    numVoted_currentChoice = 0;
                }
                
                var value: Double = Double(numVoted_currentChoice) / Double(self.numVoted[indexPath.row]) * 100
                                
                //Set value to 0 is it is undefined for asthetic reasons
                if ( (value.isNaN) || (selectedChoiceIndex ?? -1 < 0) ){
                    value = 0
                }
                //Make it positive, if value ie negative
                else if (value < 0) {
                    value = value * -1;
                }
            
                cell.lblPercentageOption4.text = String(format: "%.1f", value) + " %"
                cell.lblPercentageOption4.textColor = self.hexStringToUIColor(hex: "#0B090C")
                cell.lblPercentageOption4.textAlignment = .right
                
                //Set the Progress Bar
                cell.pgOption4.layer.cornerRadius = cell.pgOption4.frame.height / 2
                cell.pgOption4.clipsToBounds = true
                cell.pgOption4.layer.masksToBounds = true
                cell.pgOption4.setProgress(Float(value / 100), animated: true)
                
                //Register the Button
                cell.option4Button.layer.cornerRadius = cell.option4Button.frame.height / 2
                cell.option4Button.clipsToBounds = true
                cell.option4Button.layer.masksToBounds = true

                //Tint Color
                cell.pgOption4.progressTintColor = self.hexStringToUIColor(hex: "#1FD8B9")
                
                //Set the asthetics for if this option as selected
                if (selectedChoiceIndex == i) {
                    cell.imgOptionSelect4.isHidden = false;
                    cell.lblThankyou.text = "Thank you for your reponse, \(String(format: "%.1f", value))% chose this option"
                    cell.lblThankyou.isHidden = false;
                    cell.option4Button.layer.borderWidth = 2
                    
                    //Disable being able to select same one again
                    cell.option1Button.isEnabled = true
                    cell.option2Button.isEnabled = true
                    cell.option3Button.isEnabled = true
                    cell.option4Button.isEnabled = false
                } else {
                    cell.imgOptionSelect4.isHidden = true;
                    cell.option4Button.layer.borderWidth = 1
                }
            }
        }
        
        cell.likesLabel.text = String(thisQuestionModel.likes)
        cell.commentsLabel.text = String(thisQuestionModel.comments)
        
        if self.userQuestionsAnsweredModel[indexPath.row].liked {
                cell?.likeImageView.image = UIImage(named: "heart_filled_teal")
        }
        else {
                cell?.likeImageView.image = UIImage(named: "heart")
        }
                
        //Used to identify which poll we were working with
        cell?.likesButton.tag = indexPath.row
        cell?.commentsButton.tag = indexPath.row
        cell?.shareButton.tag = indexPath.row
        cell?.learnMore.tag = indexPath.row
        
        cell?.option1Button.tag = indexPath.row
        cell?.option2Button.tag = indexPath.row
        cell?.option3Button.tag = indexPath.row
        cell?.option4Button.tag = indexPath.row
        
        //Add Button Listeners
        cell?.option1Button.addTarget(self, action: #selector(option1ButtonPresed(sender:)), for: .touchUpInside)
        cell?.option2Button.addTarget(self, action: #selector(option2ButtonPresed(sender:)), for: .touchUpInside)
        cell?.option3Button.addTarget(self, action: #selector(option3ButtonPresed(sender:)), for: .touchUpInside)
        cell?.option4Button.addTarget(self, action: #selector(option4ButtonPresed(sender:)), for: .touchUpInside)
        cell?.likesButton.addTarget(self, action: #selector(likesButtonPresed(sender:)), for: .touchUpInside)
        cell?.commentsButton.addTarget(self, action: #selector(commentsButtonPresed(sender:)), for: .touchUpInside)
        cell?.shareButton.addTarget(self, action: #selector(shareButtonPresed(sender:)), for: .touchUpInside)
        cell?.learnMore.addTarget(self, action: #selector(learnMoreButtonPressed(sender:)), for: .touchUpInside)
        
        return cell
    
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return UITableView.automaticDimension
    }
    
    
        
    func setupSideMenu(){
            
            let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            
            SideMenuManager.default.menuFadeStatusBar = false
            
            let menuLeftNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
            SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
            SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
            SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
            SideMenuManager.default.menuFadeStatusBar = false
            SideMenuManager.default.menuAnimationBackgroundColor = UIColor.white
            SideMenuManager.default.menuPresentMode = .menuSlideIn
            SideMenuManager.default.menuWidth = UIScreen.main.bounds.size.width
            
    }
    
    @objc func learnMoreButtonPressed(sender: UIButton) {
        
        print(sender.tag)
        
        // Prepare the popup assets
        let title = self.questionsModel[sender.tag].title
        let message = self.questionsModel[sender.tag].learnMore
        let external_link = self.questionsModel[sender.tag].link
    
        // Create the dialog
        let popup = PopupDialog(title: title, message: message)

        // Close the Dialog
        let buttonOne = CancelButton(title: "Close") {
        }

        //Check to see if we have a link
        if (external_link!.count > 0) {
        
            // Open the blog
            let buttonTwo = DefaultButton(title: "More Information", dismissOnTap: false) {
                
                //Open Browser with URL
                if let url = URL(string: external_link ?? "") {
                    UIApplication.shared.open(url)
                }
            }
            
            popup.addButtons([buttonOne, buttonTwo])
        }
        
        //No link provided
        else {
            
            popup.addButtons([buttonOne])
        }

        // Present dialog
        self.present(popup, animated: true, completion: nil)
    }
    
    @objc func shareButtonPresed(sender: UIButton) {
        
        //Hard Coded
        let option1 = self.questionsModel[sender.tag].options[0] as? [String:Any]
        let option1_content = option1!["content"] as? String
        
        let option2 = self.questionsModel[sender.tag].options[1] as? [String:Any]
        let option2_content = option2!["content"] as? String
        
        let option3 = self.questionsModel[sender.tag].options[2] as? [String:Any]
        let option3_content = option3!["content"] as? String
        
        let option4 = self.questionsModel[sender.tag].options[3] as? [String:Any]
        let option4_content = option4!["content"] as? String
        
        let text = "Question: \(self.questionsModel[sender.tag].title ?? "")\n\nOption 1: \(option1_content ?? "")\n\nOption 2: \(option2_content ?? "")\n\nOption 3: \(option3_content ?? "")\n\nOption 4: \(option4_content ?? "")"
        let textToShare = [ text ]
        
        let shareLinkContent = ShareLinkContent()
        shareLinkContent.contentURL = URL(string: "https://polliticly.page.link/open")!
        shareLinkContent.quote = text
        
        // Share the content (photo) as a dialog with News Feed / Story
        let sharingDialog = ShareDialog(fromViewController: self, content: shareLinkContent, delegate: nil)
        sharingDialog.show()
        

        
        /*let activityViewController = UIActivityViewController(activityItems: textToShare, applicationActivities: nil)
 //       activityViewController.popoverPresentationController?.sourceView = self.view
       activityViewController.excludedActivityTypes = [ UIActivity.ActivityType.airDrop, UIActivity.ActivityType.postToFacebook ]
     //   self.present(activityViewController, animated: true, completion: nil)
        
    
            if let popoverController = activityViewController.popoverPresentationController {
                popoverController.sourceRect = CGRect(x: UIScreen.main.bounds.width / 2, y: UIScreen.main.bounds.height / 2, width: 0, height: 0)
                popoverController.sourceView = self.view
                popoverController.permittedArrowDirections = UIPopoverArrowDirection(rawValue: 0)
            }

            self.present(activityViewController, animated: true, completion: nil)*/
    }

    
    @objc func option1ButtonPresed(sender: UIButton) {
        
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : self.questionsModel[sender.tag].id!,
                          "selection"  : self.questionsModel[sender.tag].option1!] as [String : Any]
        
        //create the url with URL
        let url = URL(string: Constants.SET_POLL_SELECTION)!

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Is this the first selection the user has made on this poll? If so, increment counter
        if (self.questionsModel[sender.tag].user_selection == -1) {
            self.numVoted[sender.tag] = self.numVoted[sender.tag] + 1;
        }
        //Remove one vote from the old selection if voter has decided to change their vote
        else {
            let oldSelection = self.questionsModel[sender.tag].user_selection
            
            let oldSelectionIndex = returnPollSelectionIndex(select: oldSelection!, optionsArray: self.questionsModel[sender.tag].options)
            
            //Reduce the number of votes this option has from the user updated selection
            var optionsObject = self.questionsModel[sender.tag].options[oldSelectionIndex] as? [String: Any]
            let newTotal: Int = optionsObject?["num_votes"] as! Int - 1
            optionsObject?["num_votes"] = newTotal
            
            //Re-assignment
            self.questionsModel[sender.tag].options[oldSelectionIndex] = optionsObject
        }
        //Set the new User Selection
        self.questionsModel[sender.tag].user_selection = Int(self.questionsModel[sender.tag].option1!)
            
        //Set the Index to be 0 (first choice) of which the choice ID corresponds to
        let selection_index = 0;
        
        //Set the index on the selfQuestionsAnswerModel
        self.userQuestionsAnsweredModel[sender.tag].optionSelected = String(selection_index)
        
        //Update the number of votes this option recieved
        var optionsObject = self.questionsModel[sender.tag].options[0] as? [String: Any]
        let newTotal: Int = optionsObject?["num_votes"] as! Int + 1
        optionsObject?["num_votes"] = newTotal
        
        //Re-assignment
        self.questionsModel[sender.tag].options[0] = optionsObject
        
        self.getSelectedOptions()
    }
    
    @objc func option2ButtonPresed(sender: UIButton) {
        
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : self.questionsModel[sender.tag].id,
                          "selection"  : self.questionsModel[sender.tag].option2] as [String : Any]
        
        //create the url with URL
        let url = URL(string: Constants.SET_POLL_SELECTION)!

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Is this the first selection the user has made on this poll? If so, increment counter
        if (self.questionsModel[sender.tag].user_selection == -1) {
            self.numVoted[sender.tag] = self.numVoted[sender.tag] + 1;
        }
        //Remove one vote from the old selection if voter has decided to change their vote
        else {
            let oldSelection = self.questionsModel[sender.tag].user_selection
            
            let oldSelectionIndex = returnPollSelectionIndex(select: oldSelection!, optionsArray: self.questionsModel[sender.tag].options)
            
            //Reduce the number of votes this option has from the user updated selection
            var optionsObject = self.questionsModel[sender.tag].options[oldSelectionIndex] as? [String: Any]
            let newTotal: Int = optionsObject?["num_votes"] as! Int - 1
            optionsObject?["num_votes"] = newTotal
            
            //Re-assignment
            self.questionsModel[sender.tag].options[oldSelectionIndex] = optionsObject
        }
        //Set the new User Selection
        self.questionsModel[sender.tag].user_selection = Int(self.questionsModel[sender.tag].option1!)
            
        //Set the Index to be 1 (second choice) of which the choice ID corresponds to
        let selection_index = 1;
        
        //Set the index on the selfQuestionsAnswerModel
        self.userQuestionsAnsweredModel[sender.tag].optionSelected = String(selection_index)
        
        //Update the number of votes this option recieved
        var optionsObject = self.questionsModel[sender.tag].options[1] as? [String: Any]
        let newTotal: Int = optionsObject?["num_votes"] as! Int + 1
        optionsObject?["num_votes"] = newTotal
        
        //Re-assignment
        self.questionsModel[sender.tag].options[1] = optionsObject
        
        self.getSelectedOptions()
        
    }
    @objc func option3ButtonPresed(sender: UIButton) {
        
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : self.questionsModel[sender.tag].id,
                          "selection"  : self.questionsModel[sender.tag].option3] as [String : Any]
        
        //create the url with URL
        let url = URL(string: Constants.SET_POLL_SELECTION)!

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Is this the first selection the user has made on this poll? If so, increment counter
        if (self.questionsModel[sender.tag].user_selection == -1) {
            self.numVoted[sender.tag] = self.numVoted[sender.tag] + 1;
        }
        //Remove one vote from the old selection if voter has decided to change their vote
        else {
            let oldSelection = self.questionsModel[sender.tag].user_selection
            
            let oldSelectionIndex = returnPollSelectionIndex(select: oldSelection!, optionsArray: self.questionsModel[sender.tag].options)
            
            //Reduce the number of votes this option has from the user updated selection
            var optionsObject = self.questionsModel[sender.tag].options[oldSelectionIndex] as? [String: Any]
            let newTotal: Int = optionsObject?["num_votes"] as! Int - 1
            optionsObject?["num_votes"] = newTotal
            
            //Re-assignment
            self.questionsModel[sender.tag].options[oldSelectionIndex] = optionsObject
        }
        //Set the new User Selection
        self.questionsModel[sender.tag].user_selection = Int(self.questionsModel[sender.tag].option1!)
            
        //Set the Index to be 2 (third choice) of which the choice ID corresponds to
        let selection_index = 2;
        
        //Set the index on the selfQuestionsAnswerModel
        self.userQuestionsAnsweredModel[sender.tag].optionSelected = String(selection_index)
        
        //Update the number of votes this option recieved
        var optionsObject = self.questionsModel[sender.tag].options[2] as? [String: Any]
        let newTotal: Int = optionsObject?["num_votes"] as! Int + 1
        optionsObject?["num_votes"] = newTotal
        
        //Re-assignment
        self.questionsModel[sender.tag].options[2] = optionsObject
        
        self.getSelectedOptions()
        
    }
    
    @objc func option4ButtonPresed(sender: UIButton) {
        
        
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : self.questionsModel[sender.tag].id,
                          "selection"  : self.questionsModel[sender.tag].option4] as [String : Any]
        
        //create the url with URL
        let url = URL(string: Constants.SET_POLL_SELECTION)!

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Is this the first selection the user has made on this poll? If so, increment counter
        if (self.questionsModel[sender.tag].user_selection == -1) {
            self.numVoted[sender.tag] = self.numVoted[sender.tag] + 1;
        }
        //Remove one vote from the old selection if voter has decided to change their vote
        else {
            let oldSelection = self.questionsModel[sender.tag].user_selection
            
            let oldSelectionIndex = returnPollSelectionIndex(select: oldSelection!, optionsArray: self.questionsModel[sender.tag].options)
            
            //Reduce the number of votes this option has from the user updated selection
            var optionsObject = self.questionsModel[sender.tag].options[oldSelectionIndex] as? [String: Any]
            let newTotal: Int = optionsObject?["num_votes"] as! Int - 1
            optionsObject?["num_votes"] = newTotal
            
            //Re-assignment
            self.questionsModel[sender.tag].options[oldSelectionIndex] = optionsObject
        }
        //Set the new User Selection
        self.questionsModel[sender.tag].user_selection = Int(self.questionsModel[sender.tag].option1!)
            
        //Set the Index to be 3 (last choice) of which the choice ID corresponds to
        let selection_index = 3;
        
        //Set the index on the selfQuestionsAnswerModel
        self.userQuestionsAnsweredModel[sender.tag].optionSelected = String(selection_index)
        
        //Update the number of votes this option recieved
        var optionsObject = self.questionsModel[sender.tag].options[3] as? [String: Any]
        let newTotal: Int = optionsObject?["num_votes"] as! Int + 1
        optionsObject?["num_votes"] = newTotal
        
        //Re-assignment
        self.questionsModel[sender.tag].options[3] = optionsObject
        
        self.getSelectedOptions()
    }
    
    /**
     * When the User Clicks on the Like Button for each Poll
     */
    @objc func likesButtonPresed(sender: UIButton) {
        var numberOfLikes: Int = 0
        
        var url: URL
                
        //Set and unset Liked Statused on UI
        if self.userQuestionsAnsweredModel[sender.tag].liked {
            self.userQuestionsAnsweredModel[sender.tag].liked = false
            numberOfLikes = self.questionsModel[sender.tag].likes - 1
            self.questionsModel[sender.tag].likes = numberOfLikes
         
            //Remove a like to the poll
            url = URL(string: Constants.REMOVE_LIKE_URL)!
        }
        else {
            self.userQuestionsAnsweredModel[sender.tag].liked = true
            numberOfLikes = self.questionsModel[sender.tag].likes + 1
            self.questionsModel[sender.tag].likes = numberOfLikes

            //Add a like to the poll
            url = URL(string: Constants.ADD_LIKE_URL)!
        }
                
        //Create JSON Object to send to Database
        let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                          "pollID"     : self.questionsModel[sender.tag].id!] as [String : Any]

        //Send the Request
        myHTTPRequest.openURL(parameters: parameters, url: url)
        
        //Reload the Table to show the changes
        tableQuestionsView.reloadData()
    }
    
    /*******
     *
     * This Functions activates when the Comment Button is pressed - Load a new UI Window
     *
     **/
    @objc func commentsButtonPresed(sender: UIButton) {
        
        let commentViewController = CommentViewController(nibName: String(describing: CommentViewController.self), bundle: nil)
        commentViewController.userQuestionsAnsweredSelected = self.userQuestionsAnsweredModel[sender.tag]
        commentViewController.questionSelected = self.questionsModel[sender.tag]
        commentViewController.numVoted = self.numVoted[sender.tag]
        
        self.navigationController?.pushViewController(commentViewController, animated: true)
    }
    
}


extension Notification.Name {
    static let updateDashboard = Notification.Name("updateDashboard")
    static let clenDatabaseReference = Notification.Name("clenDatabaseReference")
}

