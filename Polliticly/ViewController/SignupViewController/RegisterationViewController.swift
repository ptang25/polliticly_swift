//
//  RegisterViewController.swift
//  Polliticly
//
//  Created by Apple on 13/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import Toaster
import SVProgressHUD
import Firebase
import FirebaseAuth
import FirebaseMessaging
import iOSDropDown
import DatePickerDialog
import FirebaseDatabase
import FirebaseAuth
import FirebaseStorage

class RegisterationViewController: UIViewController {

    @IBOutlet weak var userNameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var genderTextField: DropDown!
    @IBOutlet weak var birthdayTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var ethicity: DropDown!
    @IBOutlet weak var createAccountLabel: UILabel!
    @IBOutlet weak var streetTextField: UITextField!
    @IBOutlet weak var zipCodeTextField: UITextField!
    @IBOutlet weak var houseNumTextField: UITextField!
    @IBOutlet weak var cityTextField: UITextField!
    @IBOutlet weak var state: DropDown!
    
    @IBOutlet weak var registrationView: UIView!
    
    
    @IBOutlet weak var imageProfileView: UIImageView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //Set Dark Mode and Light Mode
        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                //Change the Background Color of the Frame
                registrationView.backgroundColor = UIColor.black;
                createAccountLabel.textColor = UIColor.white;
                
                //Change TextField Background Color, Text Color and Placeholder Color
                userNameTextField.backgroundColor = UIColor.white
                userNameTextField.textColor = UIColor.darkGray
                userNameTextField.attributedPlaceholder = NSAttributedString(string: "Username", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                emailTextField.backgroundColor = UIColor.white
                emailTextField.textColor = UIColor.darkGray
                emailTextField.attributedPlaceholder = NSAttributedString(string: "Email Address", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                zipCodeTextField.backgroundColor = UIColor.white
                zipCodeTextField.textColor = UIColor.darkGray
                zipCodeTextField.attributedPlaceholder = NSAttributedString(string: "Zip Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                houseNumTextField.backgroundColor = UIColor.white
                houseNumTextField.textColor = UIColor.darkGray
                houseNumTextField.attributedPlaceholder = NSAttributedString(string: "House Number (Ex. 315)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                streetTextField.backgroundColor = UIColor.white
                streetTextField.textColor = UIColor.darkGray
                streetTextField.attributedPlaceholder = NSAttributedString(string: "Street (Ex. West 100 Street)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                birthdayTextField.backgroundColor = UIColor.white
                birthdayTextField.textColor = UIColor.darkGray
                birthdayTextField.attributedPlaceholder = NSAttributedString(string: "Date of birth", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                passwordTextField.backgroundColor = UIColor.white
                passwordTextField.textColor = UIColor.darkGray
                passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                lastNameTextField.backgroundColor = UIColor.white
                lastNameTextField.textColor = UIColor.darkGray
                lastNameTextField.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                firstNameTextField.backgroundColor = UIColor.white
                firstNameTextField.textColor = UIColor.darkGray
                firstNameTextField.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                cityTextField.backgroundColor = UIColor.white
                cityTextField.textColor = UIColor.darkGray
                cityTextField.attributedPlaceholder = NSAttributedString(string: "City (Ex. New York City)", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                genderTextField.backgroundColor = UIColor.white
                genderTextField.textColor = UIColor.darkGray
                genderTextField.rowBackgroundColor = UIColor.darkGray
                genderTextField.attributedPlaceholder = NSAttributedString(string: "Gender", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                ethicity.backgroundColor = UIColor.white
                ethicity.textColor = UIColor.darkGray
                ethicity.rowBackgroundColor = UIColor.darkGray
                ethicity.attributedPlaceholder = NSAttributedString(string: "Ethnicity", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                state.backgroundColor = UIColor.white
                state.textColor = UIColor.darkGray
                state.rowBackgroundColor = UIColor.darkGray
                state.attributedPlaceholder = NSAttributedString(string: "State", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            }
        }
        
        // Do any additional setup after loading the view.
        setupView()
    }
    @IBAction func profileTapped(_ sender: UIButton) {
        ImagePickerManager().pickImage(self){ image in
            //here is the image
            self.imageProfileView.image = image
            
        }
    }
    
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        let firstName = firstNameTextField.text ?? ""
        let lastName = lastNameTextField.text ?? ""
        let email = emailTextField.text ?? ""
        let birthday = birthdayTextField.text ?? ""
        let gender = genderTextField.text ?? ""
        let zipCode = zipCodeTextField.text ?? ""
        let password = passwordTextField.text ?? ""
        let userName = userNameTextField.text ?? ""
        let ethinicity = ethicity.text ?? ""
        let houseNum = houseNumTextField.text ?? ""
        let street = streetTextField.text ?? ""
        let city = cityTextField.text ?? ""
        let myState = state.text ?? ""
        
        if firstName.isEmpty || lastName.isEmpty || email.isEmpty || birthday.isEmpty || gender.isEmpty || zipCode.isEmpty || password.isEmpty || userName.isEmpty || ethinicity.isEmpty || city.isEmpty || myState.isEmpty {
            Toast(text: "Please enter all data!!!").show()
        }
        else {
            SVProgressHUD.show(withStatus: "Creating account...")
            
            //Firebase Signup
            Auth.auth().createUser(withEmail: email, password: password) { authResult, error in
                if error != nil {
                    DispatchQueue.main.async {
                        SVProgressHUD.dismiss()
                        Toast(text: error?.localizedDescription ?? "Sign Up Error!!!").show()
                    }
                }
                else {
                    let imageName = "\(email).jpg"
                    var path = ""
                    path = "profile/image\(imageName)"
                    
                    self.uploadImagePic(image: self.imageProfileView.image!, name: imageName, filePath: path, completion: { url in
                                                
                        //Sign In to Backend as well
                        let parameters = ["email" : email,
                                          "password" : password,
                                          "first_name" : firstName,
                                          "last_name"  : lastName,
                                          "birthday"   : birthday,
                                          "gender"     : gender,
                                          "zipCode"    : zipCode,
                                          "username"   : userName,
                                          "avatar"     : url!,
                                          "street"     : street,
                                          "house_num"  : houseNum,
                                          "ethnicity"  : ethinicity,
                                          "city"       : city,
                                          "state"      : myState,
                                          "deviceToken": UserDefaults.standard.string(forKey: "deviceToken") ?? "" ]
                        
                        //create the url with URL
                        let url = URL(string: Constants.EDIT_VOTER_URL)!

                        //create the session object
                        let session = URLSession.shared

                        //now create the URLRequest object using the url object
                        var request = URLRequest(url: url)
                        request.httpMethod = "POST" //set http method as POST
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.addValue("application/json", forHTTPHeaderField: "Accept")

                        do {
                            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                        } catch let error {
                                print(error.localizedDescription)
                        }
                        
                        //create dataTask using the session object to send data to the server
                        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                            guard error == nil else {
                                return
                            }

                            guard let data = data else {
                                return
                            }

                            print(response)
                            
                            do {
                                //create json object from data
                                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                
                                    //Get the return Code of the HTTP Request
                                    let code = json["code"] as? Int;
                                        
                                    //Successfully able to retrieve user from Database
                                    if (code == 1) {
                                        let data = json["data"] as! [String:Any];
                                            
                                        //Set User Values
                                        UserDefaults.standard.setValue(data["id"], forKey: "userID")
                                        UserDefaults.standard.setValue(data["first_name"], forKey: "first_name")
                                        UserDefaults.standard.setValue(data["last_name"], forKey: "last_name")
                                        UserDefaults.standard.setValue(data["username"], forKey: "username")
                                        UserDefaults.standard.setValue(data["birthday"], forKey: "birthday")
                                        UserDefaults.standard.setValue(data["ethnicity"], forKey: "ethnicity")
                                        UserDefaults.standard.setValue(data["gender"], forKey: "gender")
                                        UserDefaults.standard.setValue(data["avatar"], forKey: "avatar")
                                        UserDefaults.standard.setValue(data["house_num"], forKey: "house_num")
                                        UserDefaults.standard.setValue(data["zipCode"], forKey: "zipCode")
                                        UserDefaults.standard.setValue(data["street"], forKey: "street")
                                        UserDefaults.standard.setValue(data["city"], forKey: "city")
                                        UserDefaults.standard.setValue(data["state"], forKey: "state")
                                            
                                    }
                                    
                                }
                            } catch let error {
                                    print(error.localizedDescription)
                            }
                        })
                        
                        task.resume()
                        
                        UserDefaults.standard.setValue(email, forKey: "email")
                        UserDefaults.standard.setValue(password, forKey: "password")
                        DispatchQueue.main.async {
                            SVProgressHUD.dismiss()
                            appDelegate.loadDashboardViewController()
                        }
                        
                    })
                    
                }
            }
        }
    }
    
    
    func setupView(){
        emailTextField.setLeftPaddingPoints(8)
        passwordTextField.setRightPaddingPoints(8)
        passwordTextField.setLeftPaddingPoints(8)
        emailTextField.setRightPaddingPoints(8)
        firstNameTextField.setLeftPaddingPoints(8)
        lastNameTextField.setRightPaddingPoints(8)
        lastNameTextField.setLeftPaddingPoints(8)
        firstNameTextField.setRightPaddingPoints(8)
        cityTextField.setRightPaddingPoints(8)
        cityTextField.setLeftPaddingPoints(8)
        birthdayTextField.setLeftPaddingPoints(8)
        genderTextField.setRightPaddingPoints(8)
        genderTextField.setLeftPaddingPoints(8)
        birthdayTextField.setRightPaddingPoints(8)
        zipCodeTextField.setRightPaddingPoints(8)
        zipCodeTextField.setLeftPaddingPoints(8)
        state.setRightPaddingPoints(8)
        state.setLeftPaddingPoints(8)
        
        genderTextField.optionArray = ["Male", "Female", "Non-Binary", "Other", "Prefer Not to Say"]
        //spokenGenderTextField.rowBackgroundColor = UIColor.white
        genderTextField.didSelect{(selectedText , index ,id) in
            self.genderTextField.text = selectedText
        }
        
        ethicity.optionArray = ["Asian / Pacific Islander", "Black or African American", "Hispanic or Latino", "Native American or American Indian", "White","Other", "Prefer Not to Say"]
        ethicity.arrowColor = self.hexStringToUIColor(hex: "#1FD8B9")
        //spokenGenderTextField.rowBackgroundColor = UIColor.white
        ethicity.didSelect{(selectedText , index ,id) in
            self.ethicity.text = selectedText
        }
        
        state.optionArray = ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut", "Delaware", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland", "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York", "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]
        state.arrowColor = self.hexStringToUIColor(hex: "#1FD8B9")
        state.didSelect{(selectedText , index ,id) in
                self.state.text = selectedText
        }
    }

    
    @IBAction func birthdayTextFieldEditingBegin(_ sender: Any) {
        
        DatePickerDialog().show("Select Date of Birth", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) {
            (date) -> Void in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "MM/dd/yyyy"
                self.birthdayTextField.text = formatter.string(from: dt)

            }
        }
        
        //Disable Keyboard from appearing
        self.view.endEditing(true)
    }
    
    
    func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }

        if ((cString.count) != 6) {
            return UIColor.gray
        }

        var rgbValue:UInt64 = 0
        Scanner(string: cString).scanHexInt64(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
   

    func uploadImagePic(image: UIImage, name: String, filePath: String,completion: @escaping (_ url: String?) -> Void) {
        guard let imageData: Data = image.jpegData(compressionQuality: 0.1) else {
            return
        }

       

        let storageRef = Storage.storage().reference(withPath: filePath)

        
        storageRef.putData(imageData, metadata: nil) { (metadata, err) in
         storageRef.downloadURL { (url, error) in

            if error != nil {
                print("Failed to download url:", error!)
                 completion(nil)
                return
            }

            let imageUrl = "\(String(describing: url))"
            completion(url?.absoluteString)
                // postRef.child(autoID).setValue(imageUrl)
             }
        }
    }
    
    
}
