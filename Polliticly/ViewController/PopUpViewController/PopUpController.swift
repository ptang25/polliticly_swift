//
//  PopUpController.swift
//  Polliticly
//
//  Created by Zan on 2021-02-13.
//  Copyright © 2021 Future Vision Tech. All rights reserved.
//

import UIKit

class PopUpController: UIViewController {

    @IBOutlet weak var popupTitle: UILabel!
    @IBOutlet weak var content: UITextView!
    @IBOutlet weak var closeButton: UIButton!
    
    let BorderWidth: CGFloat = 2.0

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.layer.cornerRadius = 16
        view.backgroundColor = UIColor.black.withAlphaComponent(0.75)

        /*modalTransitionStyle = .crossDissolve
        modalPresentationStyle = .overFullScreen
        
        // Popup Background
        self.view.layer.borderWidth = BorderWidth
        self.view.layer.masksToBounds = true
        self.view.layer.borderColor = UIColor.white.cgColor
                
        // Popup Title
        popupTitle.layer.masksToBounds = true
        popupTitle.adjustsFontSizeToFitWidth = true
        popupTitle.clipsToBounds = true
        popupTitle.font = UIFont.systemFont(ofSize: 25.0, weight: .bold)
        popupTitle.textAlignment = .center
                
        // Popup Text
        content.font = UIFont.systemFont(ofSize: 18.0, weight: .semibold)
        content.textAlignment = .center
        
        popupTitle.text = title
        content.text = "text"*/
        
        closeButton.addTarget(self, action: #selector(dismissView), for: .touchUpInside)
        
        moveIn()
    }
    
    @objc func dismissView(){
            moveOut()
    }
    
    func moveIn() {
        self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
        self.view.alpha = 0.0
        
        UIView.animate(withDuration: 0.24) {
            self.view.transform = CGAffineTransform.identity
            self.view.alpha = 1.0
        }
    }

    func moveOut() {
        UIView.animate(withDuration: 0.24, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.35, y: 1.35)
            self.view.alpha = 0.0
        }) { _ in
            self.view.removeFromSuperview()
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
