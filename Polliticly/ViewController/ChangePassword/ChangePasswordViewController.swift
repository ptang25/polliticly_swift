//
//  ChangePasswordViewController.swift
//  Polliticly
//
//  Created by Apple on 16/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import UIKit
import Toaster
import SVProgressHUD
import Firebase
import FirebaseAuth
import FirebaseMessaging
import iOSDropDown
import DatePickerDialog
class ChangePasswordViewController: UIViewController {

    @IBOutlet weak var tfCurrentPass: UITextField!
    
    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var tfNewPassword: UITextField!
    typealias Completion = (Error?) -> Void

    override func viewDidLoad() {
        super.viewDidLoad()

        //Set for Dark Mode
        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                
                //Change Background
                self.viewContent.backgroundColor = UIColor.black;
                
                //Change Text Fields
                self.tfCurrentPass.backgroundColor = UIColor.white
                self.tfCurrentPass.textColor = UIColor.darkGray
                self.tfCurrentPass.attributedPlaceholder = NSAttributedString(string: "Current Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                self.tfNewPassword.backgroundColor = UIColor.white
                self.tfNewPassword.textColor = UIColor.darkGray
                self.tfNewPassword.attributedPlaceholder = NSAttributedString(string: "New Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
            }
        }
        // Do any additional setup after loading the view.
        steupView()
    }

    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updatePasswordTapped(_ sender: Any) {
        
        SVProgressHUD.show()
        
        self.changePassword(email: UserDefaults.standard.string(forKey: "email") ?? "", currentPassword: tfCurrentPass.text!, newPassword: tfNewPassword.text!) { (error) in
            if error != nil {
                SVProgressHUD.dismiss()
                Toast(text: error?.localizedDescription).show()
            }
            else {
                //Save Changes to Database
                let parameters = ["userID"            : UserDefaults.standard.integer(forKey: "userID"),
                                  "old_password"      : self.tfCurrentPass.text!,
                                  "new_password"      : self.tfNewPassword.text!] as [String : Any]
                
                //create the url with URL
                let url = URL(string: Constants.CHANGE_PASSWORD_URL)!

                //create the session object
                let session = URLSession.shared

                //now create the URLRequest object using the url object
                var request = URLRequest(url: url)
                request.httpMethod = "POST" //set http method as POST
                request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                request.addValue("application/json", forHTTPHeaderField: "Accept")

                do {
                    request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                } catch let error {
                        print(error.localizedDescription)
                }
                
                //create dataTask using the session object to send data to the server
                let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                    guard error == nil else {
                        return
                    }

                    guard let data = data else {
                        return
                    }

                    print(response)
                    
                    do {
                        //create json object from data
                        if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                        
                            //Get the return Code of the HTTP Request
                            let code = json["code"] as? Int;
                                                        
                            //Successfully able to change password
                            if (code == 1) {
                                    
                                //Updating Password
                                UserDefaults.standard.setValue(self.tfNewPassword.text, forKey: "password")
                            }
                        }
                    } catch let error {
                            print(error.localizedDescription)
                    }
                })
                
                task.resume()
                
                SVProgressHUD.dismiss()
                Toast(text: "Password changed successfully.").show()
                
                //Show the main polling screen
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
        
    }
    
    func changePassword(email: String, currentPassword: String, newPassword: String, completion: @escaping (Error?) -> Void) {
        let credential = EmailAuthProvider.credential(withEmail: email, password: currentPassword)
        Auth.auth().currentUser?.reauthenticate(with: credential, completion: { (result, error) in
            if let error = error {
                completion(error)
            }
            else {
                Auth.auth().currentUser?.updatePassword(to: newPassword, completion: { (error) in
                    completion(error)
                })
            }
        })
    }
    
    func steupView(){
        viewContent.layer.cornerRadius = 24
        viewContent.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]
        tfCurrentPass.setLeftPaddingPoints(8)
        tfNewPassword.setRightPaddingPoints(8)
    }
    
}
