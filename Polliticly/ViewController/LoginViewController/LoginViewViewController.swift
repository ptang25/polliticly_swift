//
//  LoginViewViewController.swift
//  Polliticly
//
//  Created by Apple on 13/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import UIKit
import SVProgressHUD
import Firebase
import FirebaseAuth
import Toaster
import FirebaseDatabase
class LoginViewViewController: UIViewController {
    @IBOutlet weak var viewContent: UIView!

    @IBOutlet weak var signInLabel: UILabel!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    
    //Entire View Canvas
    @IBOutlet weak var loginView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Set Dark Mode
        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                //Change the Background Color of the Frame
                viewContent.backgroundColor = UIColor.black
                loginView.backgroundColor = UIColor.black
                
                //Change Password Field Colors
                passwordTextField.backgroundColor = UIColor.white
                passwordTextField.textColor = UIColor.darkGray
                passwordTextField.attributedPlaceholder = NSAttributedString(string: "Password",
                                                                             attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
                
                emailTextField.backgroundColor = UIColor.white
                emailTextField.textColor = UIColor.darkGray
                emailTextField.attributedPlaceholder = NSAttributedString(string: "Email",
                                                                                 attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
            }
            else {
                
            }
        }
        
        //setBorder
        passwordTextField.borderColor1 = UIColor.lightGray
        
        setupView()
        
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        loginCheck(type: "Button")
    }
    
    @IBAction func backTapped(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func createAccountTapped(_ sender: Any) {
        let viewController = RegisterationViewController(nibName: String(describing: RegisterationViewController.self), bundle: nil)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func loginCheck(type: String) {
        if type == "Auto" {
            SVProgressHUD.show(withStatus: "Login...")
            Auth.auth().signIn(withEmail: UserDefaults.standard.string(forKey: "email") ?? "", password: UserDefaults.standard.string(forKey: "password") ?? "") { (user, error) in
                if error != nil {
                    SVProgressHUD.dismiss()
                    Toast(text: "Incorrect Email or Password!!!").show()
                }
                else {
                    SVProgressHUD.dismiss()
                    //let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    //let mainViewController = storyboard.instantiateViewController(withIdentifier: "main")  -- Removed by Peter, call to Old UI
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    //appDelegate.window!.rootViewController = mainViewController     -- Removed by Peter, call to Old UI
                    appDelegate.window!.makeKeyAndVisible()
                }
            }
        }
        else {
            if emailTextField.text?.isEmpty ?? true || passwordTextField.text?.isEmpty ?? true {
                Toast(text: "Please enter email & password!!!").show()
            }
            else {
                SVProgressHUD.show(withStatus: "Login...")
                Auth.auth().signIn(withEmail: emailTextField.text!, password: passwordTextField.text!) { (user, error) in
                    if error != nil {
                        SVProgressHUD.dismiss()
                        Toast(text: "Incorrect Email or Password!!!").show()
                    }
                    else {
                        //Sign In to Backend as well
                        let parameters = ["email" : self.emailTextField.text!, "password" : self.passwordTextField.text!]
                        
                        //create the url with URL
                        let url = URL(string: Constants.LOGIN_URL)!

                        //create the session object
                        let session = URLSession.shared

                        //now create the URLRequest object using the url object
                        var request = URLRequest(url: url)
                        request.httpMethod = "POST" //set http method as POST
                        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                        request.addValue("application/json", forHTTPHeaderField: "Accept")

                        do {
                            request.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: .prettyPrinted) // pass dictionary to nsdata object and set it as request body
                        } catch let error {
                                print(error.localizedDescription)
                        }
                        
                        //create dataTask using the session object to send data to the server
                        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in

                            guard error == nil else {
                                return
                            }

                            guard let data = data else {
                                return
                            }

                            do {
                                //create json object from data
                                if let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String: Any] {
                                
                                    //Get the return Code of the HTTP Request
                                    let code = json["code"] as? Int;
                                        
                                    //Successfully able to retrieve user from Database
                                    if (code == 1) {
                                        let data = json["data"] as! [String:Any];
                                            
                                        //Set User Values
                                        UserDefaults.standard.setValue(data["id"], forKey: "userID")
                                        UserDefaults.standard.setValue(data["first_name"], forKey: "first_name")
                                        UserDefaults.standard.setValue(data["last_name"], forKey: "last_name")
                                        UserDefaults.standard.setValue(data["username"], forKey: "username")
                                        UserDefaults.standard.setValue(data["birthday"], forKey: "birthday")
                                        UserDefaults.standard.setValue(data["ethnicity"], forKey: "ethnicity")
                                        UserDefaults.standard.setValue(data["gender"], forKey: "gender")
                                        UserDefaults.standard.setValue(data["avatar"], forKey: "avatar")
                                        UserDefaults.standard.setValue(data["house_num"], forKey: "house_num")
                                        UserDefaults.standard.setValue(data["zipCode"], forKey: "zipCode")
                                        UserDefaults.standard.setValue(data["street"], forKey: "street")
                                        UserDefaults.standard.setValue(data["city"], forKey: "city")
                                        UserDefaults.standard.setValue(data["state"], forKey: "state")
                                        
                                    }
                                    
                                }
                            } catch let error {
                                    print(error.localizedDescription)
                            }
                        })
                        
                        task.resume()
                
                        UserDefaults.standard.setValue(self.emailTextField.text!, forKey: "email")
                        UserDefaults.standard.setValue(self.passwordTextField.text!, forKey: "password")
                            
                        SVProgressHUD.dismiss()
                        appDelegate.loadDashboardViewController()
                    }
                }
            }
        }
    }
    
   func setupView(){
        viewContent.layer.cornerRadius = 24
        viewContent.layer.maskedCorners = [.layerMaxXMinYCorner,.layerMinXMinYCorner]

        emailTextField.setLeftPaddingPoints(8)
        passwordTextField.setRightPaddingPoints(8)
        passwordTextField.setLeftPaddingPoints(8)
        emailTextField.setRightPaddingPoints(8)
        if !(UserDefaults.standard.string(forKey: "email")?.isEmpty ?? true) {
            loginCheck(type: "Auto")
        }
        
    }

}

