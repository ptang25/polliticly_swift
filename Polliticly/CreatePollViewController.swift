//
//  CreatePollViewController.swift
//  Polliticly
//
//  Created by tp on 6/29/20.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import SideMenu
import DSTextView
import IQKeyboardManagerSwift
import Firebase
import FirebaseDatabase
import SVProgressHUD
import Toaster

class CreatePollViewController: UIViewController {

    @IBOutlet weak var containerView: UIScrollView!
    
    @IBOutlet weak var txtOption1: UITextField!
    @IBOutlet weak var txtOption2: UITextField!
    @IBOutlet weak var txtOption3: UITextField!
    @IBOutlet weak var txtOption4: UITextField!
    @IBOutlet weak var tvQuestions: IQTextView!
    
    @IBOutlet weak var createPollLabel: UILabel!
    
    @IBOutlet weak var createTitle: UILabel!
    
    var arrSelectedZipCode = Array<String>()
    var arrSelectedGender = Array<String>()
    var arrSelectedEthinicity = Array<String>()
    var ageLimit = ""
    
    override func viewDidLoad() {
    
        super.viewDidLoad()
        
        if #available(iOS 13.0, *) {
            //Set Dark Mode Colors if in Dark Mode
            if UITraitCollection.current.userInterfaceStyle == .dark {
                    //Title doesn't change color when in Dark Mode or Regular Mode
                    self.createTitle.textColor = UIColor.white;
                    
                    //Change the Background Color of the Frame
                    self.containerView.backgroundColor = UIColor.black;
                
                    //Change Label color so it is visible
                    self.createPollLabel.textColor = UIColor.white;
                
                    //Change Text Colors
                    self.tvQuestions.borderColor1 = UIColor.lightGray;
                    self.tvQuestions.backgroundColor = UIColor.gray;
                    self.tvQuestions.textColor = UIColor.white;
                    self.tvQuestions.attributedPlaceholder = NSAttributedString(string: "Write your question here", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                
                    self.txtOption1.borderColor1 = UIColor.lightGray;
                    self.txtOption1.backgroundColor = UIColor.gray;
                    self.txtOption1.textColor = UIColor.white;
                    self.txtOption1.attributedPlaceholder = NSAttributedString(string: "Write Option 1", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
                
                    self.txtOption2.borderColor1 = UIColor.lightGray;
                    self.txtOption2.backgroundColor = UIColor.gray;
                    self.txtOption2.textColor = UIColor.white;
                    self.txtOption2.attributedPlaceholder = NSAttributedString(string: "Write Option 2", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

                    self.txtOption3.borderColor1 = UIColor.lightGray;
                    self.txtOption3.backgroundColor = UIColor.gray;
                    self.txtOption3.textColor = UIColor.white;
                    self.txtOption3.attributedPlaceholder = NSAttributedString(string: "Write Option 3", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

                    self.txtOption4.borderColor1 = UIColor.lightGray;
                    self.txtOption4.backgroundColor = UIColor.gray;
                    self.txtOption4.textColor = UIColor.white;
                    self.txtOption4.attributedPlaceholder = NSAttributedString(string: "Write Option 4", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

                    
            }
        }
        //All version under 13
        else {
            //Title doesn't change color when in Dark Mode or Regular Mode
            self.createTitle.accessibilityIgnoresInvertColors = true;
            
            self.tvQuestions.borderColor1 = UIColor.darkGray;
            self.txtOption1.borderColor1 = UIColor.darkGray;
            self.txtOption2.borderColor1 = UIColor.darkGray;
            self.txtOption3.borderColor1 = UIColor.darkGray;
            self.txtOption4.borderColor1 = UIColor.darkGray;
        }
        
        containerView.layer.cornerRadius = 25
        setupSideMenu()
        IQKeyboardManager.shared.enable = true

        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)

    }
    
    
    func setupSideMenu(){
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        SideMenuManager.default.menuFadeStatusBar = false
        
        let menuLeftNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.white
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.size.width
        
    }

    @IBAction func submitQuestionButtonPressed(_ sender: Any) {
        if tvQuestions.text.isEmpty || self.txtOption1.text?.isEmpty ?? false || self.txtOption1.text?.isEmpty ?? false || self.txtOption1.text?.isEmpty ?? false || self.txtOption1.text?.isEmpty ?? false {
            Toast(text: "All fields must be completed").show()
        }
        else {
            
            SVProgressHUD.show(withStatus: "Adding Question")
            
            let userID = UserDefaults.standard.integer(forKey: "userID")
            
            //Create JSON Object to send to Database
            let parameters = ["userID"     : userID,
                              "question"   : self.tvQuestions.text!,
                              "choice1"    : self.txtOption1.text!,
                              "choice2"    : self.txtOption2.text!,
                              "choice3"    : self.txtOption3.text!,
                              "choice4"    : self.txtOption4.text!] as [String : Any]
        

            let url = URL(string: Constants.CREATE_POLL)!
            
            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            Toast(text: "Your Question has been submitted awaiting approval").show()
            SVProgressHUD.dismiss()
            
            //Reset the fields
            self.tvQuestions.text = ""
            self.txtOption1.text = ""
            self.txtOption2.text = ""
            self.txtOption3.text = ""
            self.txtOption4.text = ""
            
            //NotificationCenter.default.post(name: .clenDatabaseReference, object: nil)
            
            
            /*let addReference = Database.database().reference().child("QuestionsInApproval").child(Database.database().reference().childByAutoId().key ?? "")
            addReference.child("title").setValue(self.tvQuestions.text ?? "")
            addReference.child("likes").setValue(0)
            addReference.child("comments").setValue(0)
            addReference.child("option1").setValue(self.txtOption1.text ?? "")
            addReference.child("option2").setValue(self.txtOption2.text ?? "")
            addReference.child("option3").setValue(self.txtOption3.text ?? "")
            addReference.child("option4").setValue(self.txtOption4.text ?? "")
            
           addReference.observeSingleEvent(of: .childAdded, with: { (snap) in
                NotificationCenter.default.post(name: .updateDashboard, object: nil)
                Toast(text: "Question is added!!!").show()
                SVProgressHUD.dismiss()
                self.tvQuestions.text = ""
                self.txtOption1.text = ""
                self.txtOption2.text = ""
                self.txtOption3.text = ""
                self.txtOption4.text = ""
            })*/
            
        }
    }
}


@IBDesignable
extension UITextField {
  
    @IBInspectable var paddingLeftCustom: CGFloat {
        get {
            return leftView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            leftView = paddingView
            leftViewMode = .always
        }
    }
    
    @IBInspectable var paddingRightCustom: CGFloat {
        get {
            return rightView!.frame.size.width
        }
        set {
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: newValue, height: frame.size.height))
            rightView = paddingView
            rightViewMode = .always
        }
    }
}
