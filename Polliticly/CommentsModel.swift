//
//  CommentsLabel.swift
//  Polliticly
//
//  Created by Future Vision Tech  on 07/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import Foundation
class CommentsModel {
    
    var id: Int!
    var name: String!      //Name of Comment Author
    var comment: String!   //The Comment
    var timeStamp: String!
    var strTimeStamp: String!
    var userID:String?     //ID of the Author
    
    
    init(id: Int, name: String, comment: String, timeStamp: String,userID:String?,strTimeStamp:String) {
        
        self.id = id
        self.name = name
        self.comment = comment
        self.timeStamp = timeStamp
        self.userID = userID
        self.strTimeStamp = strTimeStamp
    }
}
