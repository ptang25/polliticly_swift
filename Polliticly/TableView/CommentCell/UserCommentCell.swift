//
//  UserCommentCell.swift
//  Polliticly
//
//  Created by Apple on 07/08/20.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit

class UserCommentCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var lblComment: UILabel!
    @IBOutlet weak var btnDelete: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
