//
//  QuestionTableViewCell.swift
//  Polliticly
//
//  Created by Apple on 14/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit

class QuestionTableViewCell: UITableViewCell {

    
    @IBOutlet weak var titleTextView: UITextView!
    @IBOutlet weak var sponsorLabel: UILabel!
    @IBOutlet weak var option1Button: UIButton!
    @IBOutlet weak var option2Button: UIButton!
    @IBOutlet weak var option3Button: UIButton!
    @IBOutlet weak var option4Button: UIButton!
    @IBOutlet weak var commentsLabel: UILabel!
    @IBOutlet weak var likesLabel: UILabel!
    @IBOutlet weak var commentsButton: UIButton!
    @IBOutlet weak var learnMore: UIButton!
    @IBOutlet weak var likesButton: UIButton!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var likeImageView: UIImageView!
    @IBOutlet weak var lblOption1: UILabel!
    @IBOutlet weak var lblOption2: UILabel!
    @IBOutlet weak var lblOption3: UILabel!
    @IBOutlet weak var lblOption4: UILabel!
    @IBOutlet weak var lblThankyou: UILabel!
    
    @IBOutlet weak var pgOption1: UIProgressView!
    @IBOutlet weak var pgOption2: UIProgressView!
    @IBOutlet weak var pgOption3: UIProgressView!
    @IBOutlet weak var pgOption4: UIProgressView!
    
    @IBOutlet weak var lblPercentageOption1: UILabel!
    @IBOutlet weak var lblPercentageOption2: UILabel!
    @IBOutlet weak var lblPercentageOption3: UILabel!
    @IBOutlet weak var lblPercentageOption4: UILabel!
    
    @IBOutlet weak var imgOptionSelect1: UIImageView!
    @IBOutlet weak var imgOptionSelect2: UIImageView!
    @IBOutlet weak var imgOptionSelect3: UIImageView!
    @IBOutlet weak var imgOptionSelect4: UIImageView!
        
    //Array of Choices
    public var choices: [Any]? = [Any]()
    
    //User Selected Index
    public var selectedChoiceIndex: Int?
    
    //Percentage Totals from each Choice vs. the Overall
    public var pctTotal: [Double]? = [Double]()
        
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        
//        DispatchQueue.main.async {
//                          self.pgOption1.layer.cornerRadius = self.pgOption1.frame.height / 2
//                          self.pgOption2.layer.cornerRadius = self.pgOption2.frame.height / 2
//                          self.pgOption3.layer.cornerRadius = self.pgOption3.frame.height / 2
//                          self.pgOption4.layer.cornerRadius = self.pgOption4.frame.height / 2
//
//                          self.option1Button.layer.cornerRadius = self.option1Button.frame.height / 2
//                          self.option2Button.layer.cornerRadius = self.option2Button.frame.height / 2
//                          self.option3Button.layer.cornerRadius = self.option3Button.frame.height / 2
//                          self.option4Button.layer.cornerRadius = self.option4Button.frame.height / 2
//
//               }
        
    }
    

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
