//
//  MessageViewController.swift
//  Polliticly
//
//  Created by tp on 6/29/20.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import UIKit
import SideMenu
import IQKeyboardManagerSwift
import MessageUI
import Toaster

class MessageViewController: UIViewController {

    @IBOutlet weak var containerView: UIScrollView!
    @IBOutlet weak var btnCongressMember:UIButton!
    @IBOutlet weak var btnStateSenator:UIButton!
    @IBOutlet weak var btnStateAssemblyMember:UIButton!
    @IBOutlet weak var btnCityCouncilMember:UIButton!
    @IBOutlet weak var btnCommunityBoard:UIButton!
    @IBOutlet weak var btnSubmit:UIButton!
    @IBOutlet weak var tvMessage: IQTextView!
    @IBOutlet weak var messageViewLabel: UILabel!
        
    @IBOutlet weak var messageTitle: UILabel!
    let assemblyConst = "assemblyDistrict"
    let congressConst = "congressionalDistrict"
    let senatorConst  = "stateSenatorialDistrict"
    let cityCouncilConst = "cityCouncilDistrict"
    let communityConst = "communityDistrict"
    
    var arrRecipients:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if #available(iOS 13.0, *) {
            if UITraitCollection.current.userInterfaceStyle == .dark {
                self.messageTitle.textColor = UIColor.white;
                
                //Change the Background Color of the Frame
                self.containerView.backgroundColor = UIColor.black;
                
                //Make the label visible
                self.messageViewLabel.textColor = UIColor.white;
                
                //Set font color for the 4 options
                self.btnCongressMember.setTitleColor(UIColor.white, for: .normal)
                self.btnStateSenator.setTitleColor(UIColor.white, for: .normal)
                self.btnStateAssemblyMember.setTitleColor(UIColor.white, for: .normal)
                self.btnCityCouncilMember.setTitleColor(UIColor.white, for: .normal)
                self.btnCommunityBoard.setTitleColor(UIColor.white, for: .normal)
                
                //Color for Message Part
                self.tvMessage.borderColor1 = UIColor.lightGray;
                self.tvMessage.backgroundColor = UIColor.gray;
                self.tvMessage.textColor = UIColor.white;
                self.tvMessage.attributedPlaceholder = NSAttributedString(string: "Write your message to your elected officials here", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])

            }
        }
        //All version under 13
        else {
            self.messageTitle.accessibilityIgnoresInvertColors = true;
            
            self.tvMessage.borderColor1 = UIColor.darkGray;
        }
        
        containerView.layer.cornerRadius = 25
        setupSideMenu()
        IQKeyboardManager.shared.enable = true

        // Do any additional setup after loading the view.
        
    }
    
    @IBAction func menuBtnClicked(_ sender: Any) {
        present(SideMenuManager.default.menuLeftNavigationController!, animated: true, completion: nil)

    }
    
    
    @IBAction func btnCongressMemberClicked(_ sender: UIButton) {
      
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnStateSenatorClicked(_ sender: UIButton) {
      
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnStateAssemblyMemberClicked(_ sender: UIButton) {
      
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnCityCouncilMemberClicked(_ sender: UIButton) {
      
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnCommunityBoardClicked(_ sender: UIButton) {
      
        if sender.isSelected {
            sender.isSelected = false
        } else {
            sender.isSelected = true
        }
    }
    
    @IBAction func btnSubmitClicked(_ sender: UIButton) {
        
        self.arrRecipients = ["nick@polliticly.com"]
        
        //Create Array of who has been selected
        var membersToContact : [String] = [String]()
        
        if btnCongressMember.isSelected {
            membersToContact.append(self.assemblyConst)
                  // self.arrRecipients.append("CongressMember@gmail.com")
        }
        if btnStateSenator.isSelected {
            membersToContact.append(self.senatorConst)
                 //  self.arrRecipients.append("StateSenator@gmail.com")
        }
        if btnStateAssemblyMember.isSelected {
            membersToContact.append(self.assemblyConst)
                  // self.arrRecipients.append("StateAssemblyMember@gmail.com")
        }
        if btnCityCouncilMember.isSelected {
            membersToContact.append(self.cityCouncilConst)
                //  self.arrRecipients.append("CityCouncilMember@gmail.com")
        }
        if btnCommunityBoard.isSelected {
            membersToContact.append(self.communityConst)
                 //  self.arrRecipients.append("CommunityBoard@gmail.com")
        }
        
        if membersToContact.count == 0 {
            Toast(text: "Please select at least one member prior to submitting your message").show()
        } else if tvMessage.text.isEmpty {
            Toast(text: "Please enter your message").show()
        } else {
            //Create JSON Object to send to Database
            let parameters = ["userID"     : UserDefaults.standard.integer(forKey: "userID"),
                              "members"    : membersToContact,
                              "message"    : self.tvMessage.text!] as [String : Any]

            let url = URL(string: Constants.MSG_REPS)!
            
            //Send the Request
            myHTTPRequest.openURL(parameters: parameters, url: url)
            
            //Reset fields
            Toast(text: "Emails successfully sent").show()
            
            self.tvMessage.text = ""
            btnCongressMember.isSelected = false
            btnStateSenator.isSelected = false
            btnStateAssemblyMember.isSelected = false
            btnCityCouncilMember.isSelected = false
            btnCommunityBoard.isSelected = false
            
            //self.sendEmail()
        }
    }
    func sendEmail() {
        
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(self.arrRecipients)
            mail.setMessageBody(tvMessage.text, isHTML: false)
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }


    
    
    
    func setupSideMenu(){
        
        let mainStoryboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        
        SideMenuManager.default.menuFadeStatusBar = false
        
        let menuLeftNavigationController = mainStoryboard.instantiateViewController(withIdentifier: "LeftMenuNavigationController") as? UISideMenuNavigationController
        SideMenuManager.default.menuLeftNavigationController = menuLeftNavigationController
        SideMenuManager.default.menuAddPanGestureToPresent(toView: self.navigationController!.navigationBar)
        SideMenuManager.default.menuAddScreenEdgePanGesturesToPresent(toView: self.view)
        SideMenuManager.default.menuFadeStatusBar = false
        SideMenuManager.default.menuAnimationBackgroundColor = UIColor.white
        SideMenuManager.default.menuPresentMode = .menuSlideIn
        SideMenuManager.default.menuWidth = UIScreen.main.bounds.size.width
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MessageViewController : MFMailComposeViewControllerDelegate {
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           controller.dismiss(animated: true)
       }
}
