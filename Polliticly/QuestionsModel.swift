//
//  QuestionsModel.swift
//  Polliticly
//
//  Created by Future Vision Tech  on 07/06/2020.
//  Copyright © 2020 Future Vision Tech. All rights reserved.
//

import Foundation
class QuestionsModel {
    var id: String!
    var title: String!
    
    //Latest additions to Table
    var sponsorTitle: String!
    var learnMore: String!
    var link: String!
    
    var options = [Any]()
    var poll_likes = [Any]()
    var poll_comments = [Any]()
    
    var option1: String!
    var option2: String!
    var option3: String!
    var option4: String!
    var likes: Int!
    var comments: Int!
    
    //User info
    var user_liked: Bool = false
    var user_comments = [Any]()
    var user_selection: Int! = -1
    
    var optionSelected1 = 0
    var optionSelected2 = 0
    var optionSelected3 = 0
    var optionSelected4 = 0
    
    init(id: String!, title: String!, sponsorTitle: String!, learnMore: String!, link: String!, likes: Int, comments: Int, options: [Any], poll_likes: [Any], poll_comments: [Any], user_liked: Bool, user_comments: [Any], user_selection: Int) {
        self.id = id
        self.title = title
        
        self.sponsorTitle = sponsorTitle
        self.learnMore = learnMore
        self.link = link
        
        self.options = options
        self.likes = likes
        self.comments = comments
        
        self.poll_likes = poll_likes
        self.poll_comments = poll_comments
        
        self.user_liked = user_liked
        self.user_comments = user_comments
        self.user_selection = user_selection
        
        //Fill in the Variables
        for i in 0...options.count - 1 {

            let optionsObject = options[i] as? [String: Any]
            
            //ID of the choice
            let ID = optionsObject?["id"] as? Int
            
            //First Choice
            if (i == 0) {
                self.option1 = String(ID!)
            } else if (i == 1) {
                self.option2 = String(ID!)
            } else if (i == 2) {
                self.option3 = String(ID!)
            } else {
                self.option4 = String(ID!)
            }
        }
    }
    
    init(id: String, title: String, option1: String, option2: String, option3: String, option4: String, likes: Int, comments: Int) {
        self.id = id
        self.title = title
        self.option1 = option1
        self.option2 = option2
        self.option3 = option3
        self.option4 = option4
        self.likes = likes
        self.comments = comments
    }
}
